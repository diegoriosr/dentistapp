module.exports = {
  bracketSpacing: false, // espacios entre las llaves {}
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  arrowParens: 'avoid', // funcioanes flecha no requiera () patentesis si no es obligaotrio
  // modify component
  useTabs: true, // usar tabs y no espacios
  tabWidth: 4,
};
