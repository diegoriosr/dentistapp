/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react'
import { createStore } from 'redux'
import AppStack from './app/navigator/stack'
import { Provider } from 'react-redux'
import  messaging from '@react-native-firebase/messaging'
import AsyncStorage from '@react-native-async-storage/async-storage'
import PushNotification from 'react-native-push-notification'
import FlashMessage from "react-native-flash-message"

const initialState = {
   user: [{
      user: []
   }]
}

const reducer = (state = initialState, action) => {
   switch(action.type) {
      case 'setUser': return { 
         ... state,
         user: action.payload 
      }
   }
   return state
}

const store = createStore(reducer)

class App extends Component {

   async componentDidMount() 
   {
      const getToken = async () => {
         const token = await messaging().getToken()
         AsyncStorage.setItem('fcmToken', token)
      }

      const NotiPermission = () => {
         messaging().requestPermission().then((enabled) => {
            if( enabled ) {
               AsyncStorage.getItem("fcmToken").then(val => {
                  if( !val ) {
                     getToken()
                  }
               }).catch(err => console.log("error:", err))
            } else {
               messaging().requestPermission().then((wait) => {
                  if(wait) {
                     AsyncStorage.getItem("fcmToken").then(val => {
                        if( !val ) {
                           getToken()
                        }
                     }).catch(err => console.log("error:", err))
                  }
                  else
                  alert("Usuario no tiene token para notificaciones");                  
               })
            }
         }).catch( error => {
           alert("Usuario no tiene token para notificaciones");
         })
      }

      messaging().onMessage(async remoteMessage => {
         let msn = await remoteMessage
         PushNotification.localNotification({
            channelId: 'LocalNoti',
            title: msn.notification.title,
            message: msn.notification.body
         })
      });

      const createChannels = () => {
         PushNotification.createChannel({
            channelId: "LocalNoti",
            channelName: "Notificaciones DentistPocket",
            channelDescription: "A channel to categorise DentistPocket",
            playSound: false,
            soundName: "default",
            importance: 4,
            vibrate: false,
         });
      }

      NotiPermission()
      createChannels()
   }

   render() {
      return(
         <>
            <Provider store={store}>              
              <AppStack />
              <FlashMessage position="top" animated={true} /> 
            </Provider>
        </>
      );
   }
}

export default App;