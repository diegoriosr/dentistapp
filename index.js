/**
 * @format
 */

import {AppRegistry, Platform} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";
import App from './App';
import {name as appName} from './app.json';

// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
    //console.log('Message handled in the background!', remoteMessage);
});

PushNotification.configure({ 
    onNotification: function (notification) {
      //console.log( notification );
    },
    requestPermissions: Platform.OS === 'ios'
})

AppRegistry.registerComponent(appName, () => App);
