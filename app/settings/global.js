import { PermissionsAndroid } from 'react-native'
import { showMessage, hideMessage } from "react-native-flash-message"

import SendAPI from '../api/base'

export var server = "https://dentistpocket.showupweb.com";
//export var server = "http://10.1.0.206:3500/";
//
export var mode = "Test";
//
export var getNewAppoint = new SendAPI('apponint/getNewAppoint');
//
export var getAllAppoint = new SendAPI('apponint/getAllAppoint');
//
export var getBugetNextDate = new SendAPI('apponint/getBugetNextDate');
//
export var getAprovAppoint = new SendAPI('apponint/getAprovAppoint');
//
export var getNotiArrive = new SendAPI('apponint/getNotiArrive');
//
export var getAllAgenda = new SendAPI('apponint/getAllAgenda');
// Note / Fotos Citas
export var getSaveAppoint = new SendAPI('apponint/getSaveAppoint');
// Note / Fotos ppto
export var getSaveBuget = new SendAPI('apponint/getSaveBuget');
//
export var getAllIncumplidos = new SendAPI('apponint/getAllIncumplidos');
//
export var getAllApprove = new SendAPI('buget/getAllApprove');
//
export var sendNotification = new SendAPI('/user/sendNotif');
//
export var GetByToken = new SendAPI('user/GetByToken')
//
export var signin = new SendAPI('user/signin')
//
export var login = new SendAPI('user/login')
//
export var forgotPass = new SendAPI('user/password')
//
export var msgNoti = (message) => {
  showMessage({
      message: "DentistPoocket App",
      description:  message,
      type: "info",
      icon: {
          position: 'left',
          icon: 'warning',
      },
      backgroundColor: "black",
      color: "#FFF", 
      hideStatusBar: true,
      titleStyle:{ fontWeight: 'bold', fontSize: 19 },
      textStyle:{ fontSize: 16 }
    });
}
//
export var requestCameraPermission = async() => {
    try 
    {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA, {
          title: "Solicitud de permisos",
          message: "Esta App requiere acceso a la camara ",
          buttonNeutral: "Permitir mas tarde",
          buttonNegative: "Cancelar",
          buttonPositive: "Permitir"
      });

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true
      } else {
        return false
      }
    } catch (err) {
        return false
    }
};
