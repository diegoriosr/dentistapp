import React, { useLayoutEffect, useState } from 'react'
import { View, Text, Image, StyleSheet, ScrollView, Dimensions } from 'react-native'
import { TouchableOpacity} from 'react-native-gesture-handler'
import { useFocusEffect } from '@react-navigation/native'
import { List } from 'react-native-paper'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector } from 'react-redux'
import {launchCamera} from 'react-native-image-picker'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import Carousel from 'react-native-snap-carousel'
import TextResize from "rn-autoresize-font"
//assets
import icono from '../../../assets/icons/logo.png'
import { uploadFiles } from '../../../utils/uploadPhotos'
//connect DB
import {getAllAgenda, getSaveAppoint, getAprovAppoint, server, msgNoti, requestCameraPermission} from '../../../settings/global'
//component
import Input from '../../../components/input'
import Modal from '../../../components/modal'
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from '../../../components/carusel'

const agenda = (props) => {

    const navigation = props.navigation

    const company = useSelector((state) => state.user.company)
    const doctID = useSelector((state) => state.user.id)

    const [date, setDate] = useState(new Date())

    const isCarousel = React.useRef(null)
    
    const [data, setData] = useState([])
    const [expanded, setExpanded] = useState(true)
    const [notas, setNotas] = useState()
    const [visible, setVisible] = useState(false)
    const [phto, setPhto] = useState([])

    const handlePress = () => setExpanded(!expanded);

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "",
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: '#d3df4d',
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerShown: true,
            headerRight: () => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <Image
                      source={icono}
                      style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                  />
                </View>
              )
           },
        }) 
    }, [navigation])

    const getAllAppointfunc = (callback) => {
        let pdate = date == undefined ? '0' : date
        getAllAgenda.get(`company=${company}&doctID=${doctID}&pDate=${formatDate(pdate)}`).then(rpt => {
            if (rpt.status) {
                callback( rpt.data );
            } else {
                alert(rpt.data)
                callback([])
            }
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            getAllAppointfunc(function (req) {
                if(req.length > 0)
                  setData(req)
                else {
                  msgNoti("Hoy no tienes agenda")
                  setData([])
                }
            })
        }, [])
    );

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
      setDatePickerVisibility(true);
    };
  
    const hideDatePicker = () => {        
      getAllAppointfunc(function (req) {
        if(req.length > 0)
            setData(req)
        else {
            msgNoti("Hoy no tienes agenda")
            setData([])
        }
      })
      setDatePickerVisibility(false);
    };
  
    const handleConfirm = (date) => {
      setDate( date )
      hideDatePicker();    
    };
    
    const formatDate = (dates) => {
        var d =  dates == "0" ? new Date() : new Date(dates),
            now = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
        return [year, month, day].join('-')+ " " + d.getHours() + ":"+(parseInt(now.getMinutes()) < 10 ? '0'+now.getMinutes() : now.getMinutes())+":00";
    }

    return( 
        <View style={styles.container}>

           <View
             style={{
                     justifyContent: 'center',
                     marginBottom: 15, 
                     marginTop: 15,
                     flexDirection: 'row',
                   }}
           >
               <Text style={{fontSize: 28, color: "#d3df4d"}}>
                    Agenda del día
               </Text>

               <TouchableOpacity 
                  style={{position: 'relative', left: 20}}
                  onPress={showDatePicker}
                >
                    <MaterialCommunityIcons
                        name="calendar-refresh"
                        size={35}
                        color="#fff"
                        style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>
           </View>

           <DateTimePickerModal
             isVisible={isDatePickerVisible}
             mode="date"
             onConfirm={handleConfirm}
             onCancel={hideDatePicker}
             date={date}
           />

           <ScrollView>
           <View style={{width: '100%', flex: 1}}>
                <List.Section title="." titleStyle={{color: "#000", fontSize: 1}}>
                {
                    data.map((item, i) => (
                        <List.Accordion
                            style={{backgroundColor: "#000"}}
                            key={i}
                            title={ "Cita " + item.order + " " + item.userName + " " + item.lastName }
                            titleStyle={{color: "#D9DA08", fontSize: 21 }}
                            descriptionStyle={{color: "#FFF", fontSize: 16}}
                            description={(item.fecha == undefined ? item.approvedDate.slice(0,10) + " " + new Date(item.approvedDate).toLocaleTimeString().slice(0,5) : item.fecha)}
                            onPress={handlePress}
                            //expanded={expanded}
                            right={props => <List.Icon  {...props} icon={"chevron-" + (expanded ? "up" : "down")} color={(item.room == 0 ? "#D9DA08" : "#00FDFF")} />}
                            left={props => <List.Icon  {...props} icon="calendar" color={(item.room == 0 ? "#D9DA08" : "#00FDFF")} />}>
                            <List.Item
                                title={"Resumen de la cita"}
                                titleStyle={{color: "#D9DA08", marginBottom: 15, marginTop: -20, alignSelf: 'center', fontSize: 19 }}
                                style={{backgroundColor: "#000", fontSize: 20, marginLeft: -40}}
                                descriptionStyle={{flex: 1,width:'100%', marginLeft: -10}}
                                description={
                                    <View style={{width: '100%'}}>
                                        
                                        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                                           
                                            <TouchableOpacity
                                                onPress={async () => 
                                                {
                                                    let rp = await requestCameraPermission();                                                    
                                                    if(rp) 
                                                    {
                                                        let options = {
                                                            mediaType: 'photo', cameraType: 'front',
                                                            maxWidth: 300, maxHeight: 300, quality: 0.9,
                                                        }

                                                        launchCamera(options, (res) => {
                                                            if(!res.didCancel) {
                                                            
                                                                let body = {
                                                                    company: item.company, doctID: item.doctID,
                                                                    userName: item.userName+" "+item.lastName,
                                                                    userID: item.userID,
                                                                    bugetID: item.bugetID, appointID: item.appointID
                                                                }
                                                                
                                                                uploadFiles(res, body).then(other => 
                                                                {
                                                                    if( other.data ) 
                                                                    {
                                                                        let img = []

                                                                        if( item.photos != null ) { 
                                                                            img = JSON.parse(item.photos)
                                                                        }
                                                                        img.push({ title: (img.length + 1), body: "", imgUrl: other.photo })

                                                                        body.photos = JSON.stringify(img)

                                                                        getSaveAppoint.post( body ).then(rpt => {
                                                                            if( rpt.status ) {
                                                                                getAllAppointfunc(function(req) {
                                                                                    setData(req)
                                                                                })
                                                                            }
                                                                        }).catch(err => {
                                                                            console.log(err)
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                }}
                                            >
                                                <MaterialCommunityIcons
                                                    name="camera"
                                                    size={40}
                                                    color="#fff"
                                                    style={{color: "#d3df4d"}}
                                                />
                                            </TouchableOpacity>

                                            <TouchableOpacity
                                                onPress={() => {
                                                    let dta = JSON.parse(item.photos)
                                                    dta.forEach(async(element, ind) => {
                                                        element.imgUrl = `${server}/citas/${company}/${item.userName+" "+item.lastName}/${item.bugetID}/${item.appointID}/${element.imgUrl}`
                                                    });
                                                    setPhto(dta)
                                                    setVisible(true)
                                                }}
                                            >
                                                <MaterialCommunityIcons
                                                    name="image"
                                                    size={40}
                                                    color="#fff"
                                                    style={{color: "#d3df4d"}}
                                                />
                                            </TouchableOpacity>
                                        
                                            <TouchableOpacity
                                                onPress={() => 
                                                {
                                                    let pst = {
                                                        userID: item.userID, company,
                                                        doctID: item.doctID,
                                                        bugetID:item.bugetID,
                                                        appointID: item.appointID,
                                                        status: 3
                                                    }
        
                                                    getAprovAppoint.post( pst ).then(rpt => {
                                                        if( rpt.status ) {
                                                            getAllAppointfunc(function(req){
                                                                setData(req)
                                                            })
                                                        }
                                                    }).catch(err => {
                                                        console.log(err);
                                                    })
                                                }}
                                            >
                                                <MaterialCommunityIcons
                                                    name="checkbox-marked-outline"
                                                    size={40}
                                                    color="#fff"
                                                    style={{color: "#d3df4d"}}
                                                />
                                            </TouchableOpacity>
                                        
                                        </View>

                                        <View style={{height: 170, width: Dimensions.get('window').width - 40, marginBottom: 1}}>

                                            <TextResize 
                                                text={item.note}
                                                numberOfLines={8}
                                                style={{ color: "#FFF", textAlign: "center", width: '100%', marginTop: 10 }}
                                                fontSize={15}
                                            />

                                            <Input
                                                title="Notas de la cita"
                                                value={notas}
                                                custom={{
                                                    onChangeText: val => setNotas(val),
                                                    multiline: true,
                                                    style: {
                                                        borderWidth: 1,
                                                        borderColor: "#d3df4d",
                                                        width: '100%',
                                                        paddingHorizontal: 10,
                                                        color: "#FFF",
                                                        borderRadius: 5,
                                                        height: 100
                                                    }
                                                }}
                                            />

                                        </View>

                                        {( item.status == 2 ?                                      
                                            <TouchableOpacity
                                                style={styles.button} 
                                                onPress={() => 
                                                {
                                                    let pst = {
                                                        userID: item.userID, company,
                                                        doctID:item.doctID,
                                                        bugetID:item.bugetID,
                                                        appointID: item.appointID,
                                                        status: 4
                                                    }
                                                    getAprovAppoint.post( pst ).then(rpt => {
                                                        if( rpt.status ) {
                                                            getAllAppointfunc(function(req) {
                                                                setData(req)
                                                            })
                                                        }
                                                    }).catch(err => {
                                                        console.log(err);
                                                    })
                                                }}
                                            >
                                                <Text style={styles.title}> Iniciar </Text>
                                            </TouchableOpacity>
                                        : 
                                            <TouchableOpacity
                                                style={styles.button} 
                                                onPress={() => {
                                                    let body = {
                                                        company: item.company, doctID: item.doctID,
                                                        userID: item.userID, bugetID: item.bugetID, 
                                                        appointID: item.appointID, note: notas
                                                    }

                                                    getSaveAppoint.post( body ).then(rpt => {
                                                        if( rpt.status ) {
                                                            setNotas("")
                                                            getAllAppointfunc(function(req) {
                                                                setData(req)
                                                            })
                                                        }
                                                    }).catch(err => {
                                                        console.log(err)
                                                    })
                                                }}
                                            >
                                                <Text style={styles.title}> Guardar </Text>
                                            </TouchableOpacity> 
                                        )}

                                    </View>
                                }
                            />
                        </List.Accordion>
                    ))
                }
                </List.Section>
           </View>
           </ScrollView>

           <Modal
                visible={visible}
                onClose={() => {
                    setVisible(false)
                }}
                onShow={{}}
            >
                <View>
                    <Text style={{color: "#FFF"}}>
                       <Carousel
                            layout="default"
                            layoutCardOffset={9}
                            ref={isCarousel}
                            data={phto}
                            renderItem={CarouselCardItem}
                            sliderWidth={SLIDER_WIDTH}
                            itemWidth={ITEM_WIDTH}
                            inactiveSlideShift={0}
                            useScrollView={true}
                        />
                    </Text>
                </View>

           </Modal>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
    },
    subnontainer: {
        marginBottom:10,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        color: "white"
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    },
    button: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "100%",
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    title: {
        color: "#d3df4d",
        fontSize: 16,
        fontWeight: "bold",
        marginVertical: 8,
    },
})

export default agenda;