import React, { useState, useLayoutEffect } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    TouchableHighlight,
    TouchableOpacity,
    StatusBar,
    Image,
    TextInput,
    Dimensions,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context'
import { useSelector } from 'react-redux';
//
import { SwipeListView } from 'react-native-swipe-list-view'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
// Assets
import icono from '../../../assets/icons/logo.png'
import userImg from '../../../assets/icons/user.png'
//Data
import getAll from '../../../api/getAllPatient'

const misPacientes = ({ navigation }) => {

    const user = useSelector((state) => state.user)

    const [listData, setListData] = useState()
    const [listTemp, setListTemp] = useState()
    const [company, setCompany] = useState(user.company)

    useLayoutEffect(() => 
    {
        navigation.setOptions({
            title: "Mis Pacientes",
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: '#d3df4d',
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerShown: true,
            headerRight: () => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <Image
                      source={icono}
                      style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                  />
                </View>
              )
           },
        })

    }, [navigation])

    useFocusEffect(
      React.useCallback(() => {
        getAll.get(`company=${company}`).then(rpt => {
          if (rpt.status) {
            setListData(rpt.data)
            setListTemp(rpt.data)
          }
          else
            alert(rpt.data)
        })
      }, [])
    );

    const closeRow = (rowMap, rowKey) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = (rowMap, rowKey) => {
        closeRow(rowMap, rowKey);
        //const newData = [...listData];
        const prevIndex = listData.findIndex(item => item.key === rowKey);
        //newData.splice(prevIndex, 1);
        navigation.navigate("AddPatient", { user: listData[prevIndex] })
        //setListData(newData);
    };

    const onRowDidOpen = rowKey => {
        //console.log('This row opened', rowKey);
    };
    
    const onLeftActionStatusChange = rowKey => {
        //console.log('onLeftActionStatusChange', rowKey);
    };
    
    const onRightActionStatusChange = rowKey => {
        //console.log('onRightActionStatusChange', rowKey);
    };
    
    const onRightAction = rowKey => {
        //console.log('onRightAction', rowKey);
    };
    
    const onLeftAction = rowKey => {
        //console.log('onLeftAction', rowKey);
    };

    const VisibleItem = props => {
        const {
          data,
          rowHeightAnimatedValue,
          removeRow,
          leftActionState,
          rightActionState,
        } = props;
    
        if (rightActionState) {
          Animated.timing(rowHeightAnimatedValue, {
            toValue: 0,
            duration: 200,
            useNativeDriver: false,
          }).start(() => {
            removeRow();
          });
        }
    
        return (
          <Animated.View
            style={[styles.rowFront, {height: rowHeightAnimatedValue}]}>
            <TouchableHighlight
              style={styles.rowFrontVisible}
              onPress={() => navigation.navigate("Patient",{ user: data.item })}
              underlayColor={'#aaa'}>
              <View>
                <Text style={styles.title} numberOfLines={1}>
                  {data.item.title}
                </Text>
                <Text style={styles.details} numberOfLines={1}>
                  {data.item.details}
                </Text>
                <Image
                    source={data.item.img ? { uri: data.item.img } : userImg}
                    style={styles.icon}
                />
              </View>
            </TouchableHighlight>
          </Animated.View>
        );
    };

    const renderItem = (data, rowMap) => {
        const rowHeightAnimatedValue = new Animated.Value(60);    
        return (
          <VisibleItem
            data={data}
            rowHeightAnimatedValue={rowHeightAnimatedValue}
            removeRow={() => {
              deleteRow(rowMap, data.item.key)
            }}
          />
        );
    };

    const HiddenItemWithActions = props => {
        const {
          swipeAnimatedValue,
          leftActionActivated,
          rightActionActivated,
          rowActionAnimatedValue,
          rowHeightAnimatedValue,
          onClose,
          onDelete,
        } = props;
    
        if (rightActionActivated) {
          Animated.spring(rowActionAnimatedValue, {
            toValue: 500,
            useNativeDriver: false
          }).start();
        } else {
          Animated.spring(rowActionAnimatedValue, {
            toValue: 75,
            useNativeDriver: false
          }).start();
        }
    
        return (
          <Animated.View style={[styles.rowBack, {height: rowHeightAnimatedValue}]}>
            <Text>Left</Text>
            {!leftActionActivated && (
              <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnLeft]}
                onPress={onClose}>
                <MaterialCommunityIcons
                  name="close-circle-outline"
                  size={25}
                  style={styles.trash}
                  color="#fff"
                />
              </TouchableOpacity>
            )}
            {!leftActionActivated && (
              <Animated.View
                style={[
                  styles.backRightBtn,
                  styles.backRightBtnRight,
                  {
                    flex: 1,
                    width: rowActionAnimatedValue,
                  },
                ]}>
                <TouchableOpacity
                  style={[styles.backRightBtn, styles.backRightBtnRight]}
                  onPress={onDelete}>
                  <Animated.View
                    style={[
                      styles.trash,
                      {
                        transform: [
                          {
                            scale: swipeAnimatedValue.interpolate({
                              inputRange: [-90, -45],
                              outputRange: [1, 0],
                              extrapolate: 'clamp',
                            }),
                          },
                        ],
                      },
                    ]}>
                    <MaterialCommunityIcons
                      name="account-edit-outline"
                      size={25}
                      color="#fff"
                    />
                  </Animated.View>
                </TouchableOpacity>
              </Animated.View>
            )}
          </Animated.View>
        );
    };

    const renderHiddenItem = (data, rowMap) => {
        const rowActionAnimatedValue = new Animated.Value(75);
        const rowHeightAnimatedValue = new Animated.Value(60);
    
        return (
          <HiddenItemWithActions
            data={data}
            rowMap={rowMap}
            rowActionAnimatedValue={rowActionAnimatedValue}
            rowHeightAnimatedValue={rowHeightAnimatedValue}
            onClose={() => {
              // closeRow(rowMap, data.item.key)
            }}
            onDelete={() => {
              deleteRow(rowMap, data.item.key)
            }}
          />
        );
    };

    const _searchPatient = (value) => {

      const filteredPatiend = listTemp.filter(
        Patient => {
          
           let searched = {
            key: Patient.id,
            title: Patient.title,
            details: Patient.details,
           }

            return searched.title.indexOf(value) > -1
        })

        setListData( filteredPatiend )
    } 

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#D9DA08" barStyle="dark-content" />
            <SafeAreaView style={{backgroundColor: '#000'}}/>
            <TextInput 
              placeholder="Search ..."
              placeholderTextColor="#ddd"
              style={{
                backgroundColor: "#000",
                height:50,
                fontSize:15,
                padding: 10,
                color: "#FFF",
                marginBottom:10,
                borderBottomWidth: 0.5,
                borderBottomColor:"#D9DA08"
              }}
              onChangeText={(value) => { _searchPatient(value)} }
            />
            <TouchableOpacity 
               style={{position: 'absolute', right:0}}
               onPress={() => {
                  getAll.get(`company=${company}`).then(rpt => {
                    if (rpt.status) {
                      setListData(rpt.data)
                      setListTemp(rpt.data)
                    }
                    else
                      alert(rpt.data)
                  })
               }}
            >
              <MaterialCommunityIcons
                name="refresh"
                size={40}
                color="#fff"
                style={{marginLeft:5, color: "#d3df4d"}}
              />
            </TouchableOpacity>

            <SwipeListView
                data={listData}
                renderItem={renderItem}
                renderHiddenItem={renderHiddenItem}
                leftOpenValue={75}
                rightOpenValue={-150}
                disableRightSwipe
                onRowDidOpen={onRowDidOpen}
                leftActivationValue={100}
                rightActivationValue={-200}
                leftActionValue={0}
                rightActionValue={-500}
                //onLeftAction={onLeftAction}
                //onRightAction={onRightAction}
                //onLeftActionStatusChange={onLeftActionStatusChange}
                //onRightActionStatusChange={onRightActionStatusChange}
            />
        </View>
    );
};

export default misPacientes;

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#000',
      flex: 1,
    },
    backTextWhite: {
      color: '#FFF',
    },
    icon: {
      width: 50,
      height: 50,
      marginStart: Dimensions.get('window').width - 75,
      position: 'absolute',
      borderRadius: 25,
      borderColor: '#D9DA08',
      borderWidth: 1
    },
    rowFront: {
      backgroundColor: '#FFF',
      borderRadius: 5,
      height: 60,
      margin: 5,
      marginBottom: 1,
      shadowColor: '#999',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
    },
    rowFrontVisible: {
      backgroundColor: '#181818',
      borderRadius: 5,
      height: 60,
      padding: 10,
      marginBottom: 1,
    },
    rowBack: {
      alignItems: 'center',
      backgroundColor: '#DDD',
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: 15,
      margin: 5,
      marginBottom: 15,
      borderRadius: 5,
      height: 60,
    },
    backRightBtn: {
      alignItems: 'flex-end',
      bottom: 0,
      justifyContent: 'center',
      position: 'absolute',
      top: 0,
      width: 75,
      paddingRight: 17,
      height: 60,
    },
    backRightBtnLeft: {
      backgroundColor: '#212121',
      right: 75,
    },
    backRightBtnRight: {
      backgroundColor: '#D9DA08',
      right: 0,
      borderTopRightRadius: 5,
      borderBottomRightRadius: 5,
    },
    trash: {
      height: 25,
      width: 25,
      marginRight: 7,
    },
    title: {
      fontSize: 15,
      fontWeight: 'bold',
      marginBottom: 5,
      color: '#666',
    },
    details: {
      fontSize: 15,
      color: '#999',
    },
});
