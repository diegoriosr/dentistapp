import React, { useLayoutEffect, useState } from 'react'
import { View, Text, Image, StyleSheet, ScrollView, Dimensions } from 'react-native'
import { TouchableOpacity} from 'react-native-gesture-handler'
import { useFocusEffect } from '@react-navigation/native'
import TextResize from "rn-autoresize-font"
import { List } from 'react-native-paper'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import DatePicker from 'react-native-date-picker'
import Carousel from 'react-native-snap-carousel'
import { useSelector } from 'react-redux'
//assets
import icono from '../../../assets/icons/logo.png'
//
import {getNewAppoint, getAllAppoint, getAllApprove, sendNotification, server, msgNoti} from '../../../settings/global'
//components
import Modal from '../../../components/modal'
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from '../../../components/carusel'

const miCitas = ({route, navigation}) => {

    const company = useSelector((state) => state.user.company)
    const doctID = useSelector((state) => state.user.id)

    const isCarousel = React.useRef(null)
    
    const [data, setData] = useState([])
    const [expanded, setExpanded] = useState(true)
    const [date, setDate] = useState(new Date())
    const [visible, setVisible] = useState(false)
    const [phto, setPhto] = useState([])

    const visi = [1,2]

    const handlePress = () => setExpanded(!expanded)

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Citas por Paciente",
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: "#FFF",
            headerTitleStyle: {
                color: "#1f1f1f"
            },
            headerShown: true,
        })
 
    }, [navigation])

    const sumaFecha = (d, fecha) =>  {
        var Fecha = new Date();
        var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
        var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
        var aFecha = sFecha.split(sep);
        var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
        fecha= new Date(fecha);
        fecha.setDate(fecha.getDate()+parseInt(d));
        var anno=fecha.getFullYear();
        var mes= fecha.getMonth()+1;
        var dia= fecha.getDate();
        mes = (mes < 10) ? ("0" + mes) : mes;
        dia = (dia < 10) ? ("0" + dia) : dia;
        var fechaFinal = dia+sep+mes+sep+anno;
        return (fechaFinal);
    }

    const parseDate = (date) => {
        ///+ " " + new Date(date).getHours() + ":" + new Date(date).getMinutes() + ":" + new Date(date).getSeconds()
        return (("0" + ( new Date(date).getDate())).slice(-2) +"/"+ ("0" + ( new Date(date).getMonth() + 1)).slice(-2) +"/"+ new Date(date).getFullYear())
    }

    const formateDate = (pdate, hour) => {
       let dt = pdate.split("/")
       return dt[2]+"-"+dt[1]+"-"+dt[0]+" "+hour
    }

    const formatDate2 = (dateString) =>  {
        return dateString.toLocaleDateString() + " " +dateString.toLocaleTimeString();
    }

    const getAllAppointfunc = (callback) => {
        getAllAppoint.get(`company=${company}&userID=${route.params.patient}&doctID=${doctID}&bugetID=${route.params.buget}&type=p`).then(rpt => {
            if (rpt.status) {
                callback( rpt.data );
            } else {
                msgNoti(rpt.data)
                callback([])
            }
        })
    }

    const Notification = (token,body,callback) => {
        sendNotification.get(`company=${company}&token=${token}&body=${body}&typ=cita&usr=doctor`).then(rpt => {
            callback( rpt );
        })
    }

    const viewBuget = async(callback) => {
        getAllApprove.get(`company=${company}&doctID=${doctID}`).then(rpt => {
            if (rpt.status) {
                if(rpt.data.length) {
                    callback(true)
                }
                else
                    callback(false)
            }
            else
             msgNoti(rpt.data)
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            viewBuget(function(rp) {
                if( rp ){
                    getAllAppointfunc(function (req) {
                        if(req.length > 0)
                            setData(req)
                        else {
                            let arr = [],
                            fecha = null
                
                            for (let i = 1; i <= route.params.tCitas; i++) {
                                if( i == 1 ) {
                                    fecha = ( (new Date(route.params.fecha).getDate() + 1) +"/"+ ("0" + (new Date(route.params.fecha).getMonth() + 1)).slice(-2) +"/"+ new Date(route.params.fecha).getFullYear())
                                }else {
                                    fecha = sumaFecha(parseInt(route.params.fCitas) ,fecha)
                                }
            
                                arr.push({
                                    company,
                                    bugetID: route.params.buget,
                                    doctID,
                                    userID: route.params.patient,
                                    appointID: i,
                                    order: i,
                                    fecha,
                                    status: 0,
                                    fcmToken: route.params.fcmToken
                                })     
                            }
                            setData(arr)
                        }
                    })
                }
                else
                  msgNoti("Este Paciente no tiene presupuestos aprobados")
            })
        }, [])
    );

    return( 
        <View style={styles.container}>

           <View style={{alignContent: 'center', alignItems: 'center', marginTop: -10}} >
               <Text style={{height: 68}}>
                    <Image style={styles.img}
                           source={icono}
                    />
               </Text>
               <Text style={{color:"#FFF", marginTop: 15}}>Paciente ({route.params.nombre}) </Text>
           </View>

           <View
             style={{
                     marginBottom: 15, 
                     marginTop: 10,
                     flexWrap: 'wrap',
                     flexDirection: 'row',
                     justifyContent: 'center'
                   }}
           >
               <TouchableOpacity  onPress={() => { navigation.goBack() }} >
                    <MaterialCommunityIcons
                        name="chevron-left"
                        size={40}
                        color="#fff"
                        style={{color: "#d3df4d", right: 0}}
                    />
                </TouchableOpacity>

               <Text style={{fontSize: 28, color: "#d3df4d"}}>
                    Agendar Citas
               </Text>

           </View>

           <ScrollView>
           <View style={{width: '100%', flex: 1}}>
                <List.Section title="Seguimiento Tratamiento" titleStyle={{color: "#FFF", fontSize: 20}}>
                   {
                        data.map((item, i) => (
                            <List.Accordion
                              style={{backgroundColor: "#000"}}
                              key={i}
                              title={ "Citas " + item.order + " de " + route.params.tCitas}
                              titleStyle={{color: (item.status == 3 ? "#00FDFF" : "#D9DA08"), fontSize: 20}}
                              descriptionStyle={{color: "#FFF", fontSize: 15}}
                              description={(item.fecha == undefined ? formatDate2( new Date(item.approvedDate) ).slice(0,14) : item.fecha)}
                              onPress={handlePress}
                              right={props => <List.Icon  {...props} icon={"chevron-" + (expanded ? "up" : "down")} color="#D9DA08" />}
                              left={props => <List.Icon  {...props} icon="calendar" color={(item.status == 3 ? "#00FDFF" : "#D9DA08")} />}>
                              <List.Item
                                 title={(item.status == 3 ? "Resumen de la cita" : "Seleccionar fecha para la cita")}
                                 titleStyle={{color: "#D9DA08", marginBottom: 15, marginTop: -10, alignSelf: 'center', fontSize: 19 }}
                                 style={{backgroundColor: "#000", fontSize: 20, marginLeft: -40}}
                                 descriptionStyle={{flex: 1,width:'100%', marginLeft: -10}}
                                 description={
                                     item.status != 3 ?
                                     <>
                                        <DatePicker
                                            date={(item.approvedDate == undefined ? date : new Date(item.approvedDate))}
                                            locale='es-ES'
                                            textColor={"#D9DA08"}
                                            fadeToColor={"#000"}
                                            onDateChange={setDate}
                                        />

                                        <View style={{marginTop: 90, width: '100%'}}>

                                            <Text style={{color: "#FFF"}}> 
                                                Estado: {(item.status == 0 ? "Sin enviar" : ( item.status == 1 ? "Enviado" : ( item.status == 2 ? "Cita confirmada" : "Cita realizada" ) ) )}
                                            </Text>

                                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>

                                                {/* Enviar */}
                                                <TouchableOpacity
                                                    disabled={(item.status == 0 ? false : true )}
                                                    style={styles.button} 
                                                    onPress={() => 
                                                    {
                                                        var dtA = parseDate(date)                                                        
                                                        data.forEach((value, i) => 
                                                        { 
                                                            if( !value.status ) {
                                                                data[i].fecha = dtA
                                                                data[i].approvedDate = formateDate(dtA, date.toLocaleTimeString())
                                                                data[i].status = ( parseInt(data[i].order) == parseInt(item.order) ? 1 : 0 )
                                                                dtA = sumaFecha(parseInt( route.params.fCitas ), dtA)
                                                            }
                                                            else {
                                                                data.splice(i,1)
                                                            }                                            
                                                        })

                                                        getNewAppoint.post( data ).then(rpt => {
                                                            if( rpt.status ) {
                                                                let msg = `Se Programo su cita para el día ${date}`
                                                                msgNoti("La cita se programó con exito")
                                                                Notification(item.fcmToken,msg,function(){})
                                                                getAllAppointfunc(function(req){
                                                                    setData(req)
                                                                })
                                                            }
                                                        }).catch(err => { console.log(err); })
                                                    }}
                                                >
                                                <Text style={styles.title}> {(item.status == 0 ? "Enviar" : ( item.status == 1 ? "Enviado" : ( item.status == 2 ? "Confirmada" : "Realizada" ) ))} </Text>
                                                </TouchableOpacity>

                                                {/* Reprogramar */}
                                                {( visi.indexOf(item.status) >= 0  ?
                                                    <TouchableOpacity
                                                        disabled={(visi.indexOf(item.status) >= 0 ? false : true )}
                                                        style={styles.button} 
                                                        onPress={() => 
                                                        {
                                                            let dt = [{
                                                                company: item.company, appointID: item.appointID,
                                                                bugetID: item.bugetID, userID: item.userID,
                                                                doctID: item.doctID, status: 1,
                                                                order: item.order, approvedDate: date,
                                                                reschedule: 1, note: null, photos: null
                                                            }]

                                                            getNewAppoint.post( dt ).then(rpt => {
                                                                if( rpt.status ) 
                                                                {
                                                                    msgNoti("La cita se reprogramó con exito")

                                                                    let msg = `Se reprogramo su cita para el día ${date}`
                                                                    Notification(item.fcmToken,msg,function(){})
                                                                    
                                                                    getAllAppointfunc(function(req) {
                                                                        setData(req)
                                                                    })
                                                                }
                                                            }).catch(err => {
                                                                console.log(err);
                                                            })
                                                        }}
                                                    >
                                                        <Text style={styles.title}> {"Reprogramar"} </Text>
                                                    </TouchableOpacity>
                                                : null )}
                                                                                   
                                            </View>
                                    </View>
                                    </>
                                    :
                                    <>
                                        <View style={{width: Dimensions.get('window').width - 40, marginBottom: 1}}>

                                            <View style={{flexDirection: 'row'}}>
                                                
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        let dta = JSON.parse(item.photos)
                                                        dta.forEach(async(element, ind) => {
                                                            element.imgUrl = `${server}/citas/${company}/${item.userName+" "+item.lastName}/${item.bugetID}/${item.appointID}/${element.imgUrl}`
                                                        });
                                                        setPhto(dta)
                                                        setVisible(true)
                                                    }}
                                                >
                                                    <MaterialCommunityIcons
                                                        name="image"
                                                        size={40}
                                                        color="#fff"
                                                        style={{color: "#d3df4d"}}
                                                    />
                                                </TouchableOpacity>
                                            
                                            </View>

                                            <TextResize 
                                                text={item.note}
                                                numberOfLines={8}
                                                style={{ color: "#FFF", textAlign: "center", width: Dimensions.get('window').width - 40, marginTop: 10 }}
                                                fontSize={15}
                                            />
                                        
                                        </View>
                                    </>
                                 }
                              />
                            </List.Accordion>
                        ))
                   }
                </List.Section>
           </View>
           </ScrollView>

           <Modal
                visible={visible}
                onClose={() => {
                    setVisible(false)
                }}
                onShow={{}}
            >
                <View>
                    <Text style={{color: "#FFF"}}>
                       <Carousel
                            layout="default"
                            layoutCardOffset={9}
                            ref={isCarousel}
                            data={phto}
                            renderItem={CarouselCardItem}
                            sliderWidth={SLIDER_WIDTH}
                            itemWidth={ITEM_WIDTH}
                            inactiveSlideShift={0}
                            useScrollView={true}
                        />
                    </Text>
                </View>

            </Modal>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    subnontainer: {
        marginBottom:20,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: "white"
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    },
    button: {
        position: 'relative',
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "95%",
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginEnd: 65,
    },
    title: {
        color: "#d3df4d",
        fontSize: 16,
        fontWeight: "bold",
        marginVertical: 8,
    },
})

export default miCitas;