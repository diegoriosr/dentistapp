import React, { useLayoutEffect, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { View, Image, Text, TouchableOpacity, FlatList } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Badge } from "react-native-elements"
import { useSelector } from 'react-redux'
//assets
import { styles } from './styles'
import icono from '../../../assets/icons/logo.png'
import userImg from '../../../assets/icons/user.png'
//Component
import Modal from '../../../components/modal'
//Data
import getByID from '../../../api/getByIDPatient'
import getBuge from '../../../api/buget/getByID'

const Progreso = ({route ,navigation}) => {

   const luser = useSelector((state) => state.user)

   const [user, setUser] = useState(luser.id)
   const [company, setCompany] = useState(luser.company)
   const [visible, setVisible] = useState(false)
   const [arr, setArray] = useState([])

   const [nombre, setNombre] = useState()
   const [apellido, setApellido] = useState()
   const [documento, setDocumento] = useState()
   const [celular, setCelular] = useState()
   const [edad, setEdad] = useState()
   const [direccion, setDireccion] = useState()
   const [ciudad, setCiudad] = useState()
   const [email, setEmail] = useState()
   const [comentarios, setComentarios] = useState()
   const [avatar, setAvatar] = useState()
   const [state, setState] = useState(1)
   const [status, setStatus] = useState()
   const [loading, setLoading] = useState(false)

   useLayoutEffect(() => {

    navigation.setOptions({
        title: "Paciente",
        headerTitleAlign: 'left',
        headerStyle: {
            backgroundColor: "#1f1f1f"
        },
        headerTintColor: "#FFF",
        headerTitleStyle: {
            fontWeight: 'bold'
        },
        headerShown: true
    })

   }, [navigation])

   useFocusEffect(
    React.useCallback(() => {
       if(company && user && state) 
       {
           if( route.params.user.id != state ) 
           {
                getByID.get(`company=${company}&userID=${route.params.user.id}`).then(rpt => {
                if (rpt.status) 
                {
                    if(rpt.data.length) {
                        setState(rpt.data[0].id)
                        setNombre(rpt.data[0].userName)
                        setApellido(rpt.data[0].lastName)
                        setDocumento( rpt.data[0].resaleID.toString())
                        setCelular(rpt.data[0].phone.toString())
                        setEdad(rpt.data[0].age.toString())
                        setDireccion(rpt.data[0].adress)
                        setCiudad(rpt.data[0].city)
                        setEmail(rpt.data[0].email)
                        setAvatar(rpt.data[0].pathURL)
                    }
                }
                else
                    alert(rpt.data)
                })
                
                getBuge.get(`company=${company}&userID=${route.params.user.id}&doctID=${user}`).then(rpt => {
                    if (rpt.status)
                       setStatus("Ver")
                    else
                       setStatus("Crear")
                })
            }
       }
    }, [])
   );

   const ListHeader = () => {
    return (
      <View style={styles.headerFooterStyle}>
        <TouchableOpacity 
            style={{backgroundColor: "#000", marginTop: 6, marginLeft: 10 ,width: 150, padding: 7, borderRadius: 50}}
            onPress={() => {
                setVisible(false)
                navigation.navigate("Citas", { user: route.params.user, action: status, bugID: -1 })
            }}
        >
            <Text style={{color: "#d3df4d", fontSize: 15}}> Crear Presupuesto </Text>
        </TouchableOpacity>
      </View>
    );
   };

   const viewBuget = async(callback) => {
        let dtaTemp = []
        getBuge.get(`company=${company}&userID=${route.params.user.id}&doctID=${user}`).then(rpt => {
            if (rpt.status) {
                if(rpt.data.length) {
                    rpt.data.forEach(async (val, ind) => {                      
                        dtaTemp.push( val )
                        if( ind === rpt.data.length -1 ) {
                            callback(dtaTemp)
                        }
                    })
                }
                else
                    callback(false)
            }
            else
                alert(rpt.data)
        })
   }

   return (
    <View style={styles.container}>

           <View style={{alignContent: 'flex-end', alignItems: 'flex-end'}} >
               <Text style={{height: 80, marginTop: -25}}>
                    <Image source={icono}/>
               </Text>
           </View>

           <View
             style={{
                    marginBottom: 15, 
                    marginTop: 5,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}
           >
               <TouchableOpacity onPress={() => {  navigation.goBack()  }} >
                    <MaterialCommunityIcons
                        name="chevron-left"
                        size={40}
                        color="#fff"
                        style={{color: "#d3df4d"}}
                    />
               </TouchableOpacity>

               <Text style={{fontSize: 25, color: "#d3df4d"}}>
                    Seguimiento Paciente
               </Text>
           </View>

           <View style={styles.subnontainer}> 

                <View>
                    <Image style={styles.img}
                        source={avatar ? { uri: avatar } : userImg}
                    />

                    <Badge
                        status="success"
                        value={<Text style={{color: "#FFF"}}>T</Text>}
                        containerStyle={{ position: 'absolute', top: 5, right:25 }}
                    />
                </View>

           </View>

            {/* Botones Ver/Crear */}

            <View style={{flexDirection:"row"}}>
                <TouchableOpacity 
                    style={styles.btn}
                    onPress={() => {
                        if(status == "Crear") {
                            navigation.navigate("Citas", { user: route.params.user, action: status, bugID: -1 })
                        }
                        else {
                            viewBuget(function(rq) {
                                if(rq) {
                                    setArray(rq)
                                }
                                setVisible(true)
                            })
                        }
                    }}
                >
                        <Text style={{color: "#000", fontSize: 18, fontWeight: 'bold'}}> 
                            {status} Presupuesto 
                        </Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.btn}
                    onPress={() => {
                        viewBuget(function(rq) {
                            if(rq) {
                             let data = { 
                                buget: rq[0].bugetID, 
                                fecha: rq[0].approved,
                                tCitas: rq[0].ncitas,
                                fCitas: rq[0].fcitas,
                                nombre: nombre + " " + apellido,
                                patient: rq[0].userID,
                                fcmToken: route.params.user.fcmToken
                             }
                             navigation.navigate("CitasxPpte", data)
                            }
                        })
                    }}
                >
                    <Text style={{color: "#000", fontSize: 18, fontWeight: 'bold'}}> 
                        Agendar Cita
                    </Text>
            </TouchableOpacity>  
            </View>

            {/* Nombre */}

            <View style={{flexDirection:"row"}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Nombre:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                     {nombre}
                    </Text>
                </View>
            </View>

            {/* Apellido */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Apellido:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {apellido}
                    </Text>
                </View>
            </View>
                               
            {/* Documento */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Documento:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {documento}
                    </Text>
                </View>
            </View>

            {/* Celular */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Celular:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {celular}
                    </Text>
                </View>
            </View>

                            {/* Edad */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Edad:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {edad}
                    </Text>
                </View>
            </View>

                            {/* Dirección */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Dirección:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {direccion}
                    </Text>
                </View>
            </View>

                            {/* Ciudad */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Ciudad:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {ciudad}
                    </Text>
                </View>
            </View>

                            {/* E-mail */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      E-mail:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {email}
                    </Text>
                </View>
            </View>

                            {/* Comentarios */}

            <View style={{flexDirection:"row", marginTop: 15}}>
                <View style={{flex:1}}>
                    <Text style={{color:"#FFF", fontWeight: 'bold'}}>
                      Comentarios:
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                     style={{color:"#D9DA08", fontWeight: 'bold', marginLeft: -70}}
                     >
                      {comentarios}
                    </Text>
                </View>
            </View>

            <Modal
                visible={visible}
                onClose={() => {
                    setVisible(false)
                    setArray([])
                }}
                onShow={{}}
            >
                <View>
                    <FlatList
                        data={arr}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    backgroundColor: "#312b2d",
                                    marginTop: 7,
                                    padding: 8,
                                    height: 90,
                                    borderRadius: 10
                                }}
                            >
                                <Text style={styles.textListHeader} > 
                                  # {item.bugetID} 
                                </Text>

                                <Text style={styles.textListHeader} > 
                                    F. Creación:  {item.created}
                                </Text>

                                <Text style={styles.textListHeader} > 
                                    F. Aprobó:  {item.approved}
                                </Text>

                                <TouchableOpacity 
                                    style={styles.btnList1}
                                    onPress={() => {
                                        setVisible(false)
                                        navigation.navigate("Citas", { user: route.params.user, action: "Ver", bugID: item.bugetID })
                                    }}
                                >
                                    <Text style={styles.textList}> Editar </Text>
                                </TouchableOpacity>

                                <TouchableOpacity 
                                    style={styles.btnList2}
                                    onPress={() => {
                                        alert("Presupuesto enviado")
                                    }}
                                >
                                    <Text style={styles.textList}> Enviar </Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={styles.btnList3}>
                                    <Text style={styles.textList}> Eliminar </Text>
                                </TouchableOpacity>

                            </View>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={() => (
                            <View style={{height:10, backgroundColor:'#CCC', marginTop: -5, width: 190}} />
                        )}
                        ListEmptyComponent={() => (
                            <View style={{flex:1, marginTop: 10, color: '#FFF', fontSize: 30}}>
                                <Text> Sin Presupuestos </Text>
                            </View>
                        )}
                        ListHeaderComponent={ListHeader}
                        horizontal={false}
                        onRefresh={() => {
                            setLoading(true)
                            setTimeout(function(){ setArray([]) }, 500);
                            viewBuget(function(rq) {
                                setTimeout(function(){ 
                                    if(rq) {
                                        setArray(rq)
                                    }
                                 }, 1000);
                            })
                            setLoading(false)
                        }}
                        refreshing={loading}
                    />
                </View>

            </Modal>
    </View>
   )
}

export default Progreso