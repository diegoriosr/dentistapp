import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
        paddingVertical: 20,
        paddingHorizontal:30,
    },
    subnontainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30
    },
    img: {
        width: 180,
        height: 180,
        borderColor: "#D9DA08",
        borderWidth: 2,
        borderRadius: 90,
        //tintColor: "#FFF",   
    },
    btn: {
        backgroundColor: "#D9DA08", 
        width: "50%",
        height: 40,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 30,
        borderBottomLeftRadius: 30 
   },
   btnList1: {
        marginTop: 50,
        padding: 7,
        position: 'absolute',
        right: 3,
        backgroundColor: '#000',
        borderRadius: 5
   },
   btnList2: {
        marginTop: 50,
        padding: 7,
        position: 'absolute',
        right: 65,
        backgroundColor: '#000',
        borderRadius: 5
    },
    btnList3: {
        marginTop: 50,
        padding: 7,
        position: 'absolute',
        right: 129,
        backgroundColor: '#000',
        borderRadius: 5
   },
   textList: {
    color: "#d3df4d"
   },
   textListHeader: {
    color: "#FFF",
    fontSize: 17
   },
   emptyListStyle: {
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  itemStyle: {
    padding: 10,
  },
  headerFooterStyle: {
    width: '100%',
    height: 45,
    backgroundColor: "#312b2d",
    borderRadius: 5
  },
})