import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
        paddingVertical: 10,
        paddingHorizontal:20
    },
    iconoContainer:{
       flex: 1,
       alignContent: 'center',
       justifyContent: 'center',
       alignItems: 'center',
    },
    subnontainer: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        marginTop: 10
    },
    img: {
        width: 150,
        height: 150,
        borderRadius: 75,
        justifyContent: 'center',
        alignContent: 'center',
        //tintColor: "#FFF"
    }
})