import React, { useLayoutEffect, useState } from 'react'
import { View, Image, TouchableOpacity, ScrollView, Linking } from 'react-native'
import {launchCamera} from 'react-native-image-picker'
import { useSelector } from 'react-redux'
import { useFocusEffect } from '@react-navigation/native'
import { FloatingAction } from "react-native-floating-action"
//component
import Input from '../../../components/input'
import { uploadFiles } from '../../../utils/uploadFiles'
import { requestCameraPermission, server } from '../../../settings/global'
// RN
import createUser from '../../../api/userRN'
import getByID from '../../../api/getByIDPatient'
//assets
import { styles } from './styles'
import icono from '../../../assets/icons/logo.png'
import userImg from '../../../assets/icons/user.png'

const AddPatient = ({route, navigation, props}) => {

   const user = useSelector((state) => state.user)
   const [company, setCompany] = useState(user.company)

   const [userID, setUsrID] = useState(0)
   const [nombre, setNombre] = useState("")
   const [apellido, setApellido] = useState("")
   const [documento, setDocumento] = useState("")
   const [celular, setCelular] = useState("")
   const [edad, setEdad] = useState("")
   const [direccion, setDireccion] = useState("")
   const [ciudad, setCiudad] = useState("")
   const [email, setEmail] = useState("")
   const [comentarios, setComentarios] = useState("")
   const [avatar, setAvatar] = useState()
   const [photo, setPhoto] = useState()
   const [state, setState] = useState(0)
   const [msg, setMsg] = useState("Tú código de varificación Dentist Pocket es : {XXX}\nIniciar el app para completar el proceso")

   const actions = [{
        text: "Whatsapp",
        color: "#d3df4d",
        buttonSize: 55,
        textStyle: { fontSize: 15},
        textBackground: "#d3df4d",
        icon: require("../../../assets/icons/whatsapp.png"),
        tintColor: '#000',
        name: "bt_Whatsapp",
        position: 1
      },{
        text: "SMS",
        color: "#d3df4d",
        buttonSize: 55,
        textStyle: { fontSize: 15},
        textBackground: "#d3df4d",
        icon: require("../../../assets/icons/sms.png"),
        tintColor: '#000',
        name: "bt_Sms",
        position: 1
      },{
        text: "Correo",
        color: "#d3df4d",
        buttonSize: 55,
        textStyle: { fontSize: 15},
        textBackground: "#d3df4d",
        icon: require("../../../assets/icons/email.png"),
        tintColor: '#000',
        name: "bt_Correo",
        position: 2
     },{
      text: "Nuevo",
      color: "#d3df4d",
      buttonSize: 55,
      textStyle: { fontSize: 15},
      textBackground: "#d3df4d",
      icon: require("../../../assets/icons/new.png"),
      tintColor: '#000',
      name: "bt_new",
      position: 2
   }];

   const actionsII = [{
        text: "Correo",
        color: "#d3df4d",
        buttonSize: 55,
        textStyle: { fontSize: 15},
        textBackground: "#d3df4d",
        icon: require("../../../assets/icons/save.png"),
        tintColor: '#000',
        name: "bt_Correo",
        position: 2
    },{
    text: "Nuevo",
    color: "#d3df4d",
    buttonSize: 55,
    textStyle: { fontSize: 15},
    textBackground: "#d3df4d",
    icon: require("../../../assets/icons/new.png"),
    tintColor: '#000',
    name: "bt_new",
    position: 2
    }];

   useLayoutEffect(() => {
       navigation.setOptions({
           title: "Nuevo Paciente",
           headerTitleAlign: 'center',
           headerStyle: {
               backgroundColor: "#1f1f1f"
           },
           headerTintColor: '#d3df4d',
           headerTitleStyle: {
               fontWeight: 'bold'
           },
           headerShown: true,
           headerRight: () => {
             return (
               <View style={{flexDirection: 'row'}}>
                 <Image
                     source={icono}
                     style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                 />
               </View>
             )
          },
       })

   }, [navigation])

   const initialState = () => {
    setNombre('')
    setApellido('')
    setDocumento('')
    setCelular('')
    setEdad('0')
    setDireccion('')
    setCiudad('')
    setEmail('')
    setComentarios('')
    setAvatar(null)
    setPhoto(null)
    setState(0)
    setUsrID(0)
   };

   useFocusEffect (
    React.useCallback(() => {
      let isActive = true;
      let dataUser

      const fetchUser = async () => {
        try {
            if( route.params.user.id != "0" ) {
                dataUser = await getByID.get(`company=${company}&userID=${route.params.user.id}`).then( rpt => {
                if (rpt.status) 
                {
                    if(rpt.data.length) {
                        setUsrID(route.params.user.id)
                        setCelular(rpt.data[0].phone.toString())
                        setEdad(rpt.data[0].age.toString().trim())
                        setEmail(rpt.data[0].email.toString().trim())
                        setCiudad(rpt.data[0].city.toString().trim())
                        setAvatar(rpt.data[0].pathURL.toString().trim())
                        setNombre(rpt.data[0].userName.toString().trim())
                        setDireccion(rpt.data[0].adress.toString().trim())
                        setApellido(rpt.data[0].lastName.toString().trim())
                        setDocumento( rpt.data[0].resaleID.toString().trim())
                        setComentarios(rpt.data[0].comment.toString().trim())
                    }
                    return route.params.user.id
                }
                else
                    alert(rpt.data)
                })
            }
  
            if (isActive) {
                setState(dataUser);
            }
        } catch (e) {
          // Handle error
        }
      };
  
      if( state != route.params.user.id )
        fetchUser();
  
      return () => {
        isActive = false;
      };
    })
   );

   return(
       <View style={styles.container}>

           <ScrollView>

            <View style={styles.iconoContainer}>
                <TouchableOpacity
                    onPress={async () => {
 
                        let rp = await requestCameraPermission();

                        if(rp) {

                            let options = {
                                mediaType: 'photo', cameraType: 'front',
                                maxWidth: 300, maxHeight: 300, quality: 0.9,
                             }
                            
                            launchCamera(options, (res) => {
                                if (res.didCancel) {
                                    alert('User cancelled image picker');
                                  } else if (res.error) {
                                    alert('ImagePicker Error: ', res.error);
                                  }  else {
                                    setAvatar(res.uri)
                                    setPhoto(res)
                                  }
                            });
                        }
                    }}
                >
                    <Image
                        style={styles.img}
                        source={avatar ? { uri: avatar } : userImg}
                    />
                </TouchableOpacity>                
            </View>

            <View style={styles.subnontainer}>

                <Input
                    title="Nombre"
                    value={nombre}
                    custom={{
                        onChangeText: val => setNombre(val),
                        editable: (userID ? false : true)
                    }}
                />

                <Input
                    title="Apellido"
                    value={apellido}
                    custom={{
                        onChangeText: val => setApellido(val)
                    }}
                />

                <Input
                    title="Documento"
                    value={documento}
                    custom={{
                        onChangeText: val => setDocumento(val),
                        keyboardType:'numeric',                        
                        editable: (userID ? false : true)
                    }}
                />

                <Input
                    title="Celular"
                    value={celular}
                    custom={{
                        onChangeText: val => setCelular(val),
                        keyboardType:'numeric',
                    }}
                />

                <Input
                    title="Edad"
                    value={edad}
                    custom={{
                        onChangeText: val => setEdad(val),
                        keyboardType:'numeric',
                    }}
                />        

                <Input
                    title="Dirección"
                    value={direccion}
                    custom={{
                        onChangeText: val => setDireccion(val)
                    }}
                />

                <Input
                    title="Ciudad"
                    value={ciudad}
                    custom={{
                        onChangeText: val => setCiudad(val)
                    }}
                />

                <Input
                    title="E-mail"
                    value={email.trim()}
                    custom={{
                        autoCapitalize: 'none',
                        onChangeText: val => setEmail(val.trim()),
                        editable: (userID ? false : true)
                    }}
                />

                <Input
                    title="Comentarios"
                    value={comentarios}
                    custom={{
                        onChangeText: val => setComentarios(val),
                        multiline: true,
                        style: {
                            borderWidth: 1,
                            borderColor: "#d3df4d",
                            width: "100%",
                            paddingHorizontal: 10,
                            color: "#FFF",
                            borderRadius: 5,
                            height: 100
                        }
                    }}
                />

            </View>
            
            </ScrollView> 

            <FloatingAction
                actions={route.params.user.id != "0" ? actionsII : actions}
                iconWidth={20}
                iconHeight={20}
                buttonSize={65}
                color={"#d3df4d"}
                iconColor={"#000"}
                onPressItem={name => {
                  if( name == "bt_Whatsapp" || name == "bt_Correo" || name == "bt_Sms" ) 
                  {
                    const uri = `${server}/img/${email}/${email}.png`
                    const usr = { userName: nombre, lastName: apellido, resaleID: documento,
                                  phone :celular, age: edad, adress: direccion, city: ciudad,
                                  email, comment: comentarios, password: '', role: 'patient',
                                  company, pathURL: (avatar ? uri : "" ), status: false,
                                  userID, tipo: name == "bt_Correo" ? "c" : 'w'
                                }
                    createUser.post( usr ).then(rpt => {
                        if (rpt.status)
                        {
                            if(photo)
                              uploadFiles(photo, email)

                            alert(rpt.data.msg)
                            initialState()
                            if(name == "bt_Correo")
                              navigation.navigate("MisPacientes")
                            else {
                                let lMsg = ""
                                
                                if( name == "bt_Whatsapp" )
                                   lMsg = "https://wa.me/+57"+celular+"?text="+msg
                                else if( name == "bt_Sms" )
                                   lMsg = "sms:+57"+celular+"?body="+msg

                                Linking.openURL(lMsg.replace("{XXX}", rpt.data.auth))
                            }
                        }
                        else {
                          if( rpt.data[0].data != undefined )
                            alert(rpt.data[0].data)
                          else
                            alert(rpt.data)
                        }
                    })
                  } else {
                    navigation.setParams({ user: {id: 0} });
                    initialState()
                  }
                }}
            />
       </View>
   );
}

export default AddPatient