import React, { useEffect, useLayoutEffect, useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, Image, Dimensions, ScrollView  } from 'react-native'
import { useSelector } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { Badge } from "react-native-elements"
import Animated from 'react-native-reanimated'
import BottomSheet from 'reanimated-bottom-sheet'
import Carousel from 'react-native-snap-carousel'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'
//component $ Presupuestos
import CheckBox from 'react-native-check-box'
import RadioButton from '../../../components/radio'
import Input from '../../../components/input'
import Modal from '../../../components/modal'
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from '../../../components/carusel'
//
import buget from '../../../api/buget/getNew'
import getBuge from '../../../api/buget/getBugetID'
import {sendNotification, msgNoti, requestCameraPermission, getSaveBuget, server} from '../../../settings/global'
import { uploadPpto } from '../../../utils/uploadPhotos'
//assets
import { styles } from './styles'
import icono from '../../../assets/icons/logo.png'

const addPpto = ({route, navigation}) => {

   const Luser = useSelector((state) => state.user)

   const [load, setLoad] = useState(true)
   const [user, setUser] = useState(Luser.id)
   const [key, setkey] = useState(-1)
   const [company, setCompany] = useState(Luser.company)
   const [price, setPrice] = useState("50000")
   const [pteInfe, setPteInfe] = useState("170000")
   const [pteSupe, setPteSupe] = useState("170000")
   const [planqui, setPlanqui] = useState("225000")

   const [ncitas, setNCitas] = useState("0")
   const [total, setTotal] = useState(0)
   const [fcitas, setFCitas] = useState("8")
   const [rteInfe, setRteInfe] = useState(false)
   const [rteSupe, setRteSupe] = useState(false)
   const [blanqui, setBlanqui] = useState(false)
   const [diagnos, setDiagnos] = useState("")
   const [photos, setPhotos] = useState("")
   const [phto, setPhto] = useState([])
   const [visible, setVisible] = useState(false)
   const [state, setState] = useState(0)
   const [token, setToken] = useState()

   const PROP = [
        {
            key: '8',
            text: '8 d',
        },
        {
            key: '15',
            text: '15 d',
        },
        {
            key: '20',
            text: '20 d',
        },
        {
            key: '30',
            text: '30 d',
    },
   ];

   useLayoutEffect(() => {
       navigation.setOptions({
           title: "",
           headerTitleAlign: 'center',
           headerStyle: {
               backgroundColor: "#1f1f1f"
           },
           headerTintColor: "#FFF",
           headerTitleStyle: {
               fontWeight: 'bold'
           },
           headerShown: true,
           headerRight: () => {
            return (
              <View style={{flexDirection: 'row'}}>
                <Image
                    source={icono}
                    style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                />
              </View>
            )
         },
       })
   }, [navigation])

   useEffect(() => {
       if( !load )
          setTotal( total + ( rteInfe ? parseInt(pteInfe) : ( parseInt(pteInfe) * -1 )  ) )
   }, [rteInfe])

   useEffect(() => {
        if( !load )
           setTotal( total + ( rteSupe ? parseInt(pteSupe) : ( parseInt(pteSupe)  * -1 )  ) )
   }, [rteSupe])

   useEffect(() => {
        if( !load )
           setTotal( total + ( blanqui ? parseInt(planqui) : ( parseInt(planqui) * -1 )  ) )
   }, [blanqui])
    
   useEffect(() => {
       setTotal( (parseInt(price) * parseInt( ncitas )) + ( rteInfe ? parseInt(pteInfe == "" ? 0 : pteInfe) : 0 ) + ( rteSupe ? parseInt(pteSupe == "" ? 0 : pteSupe) : 0 ) + ( blanqui ? parseInt(planqui == "" ? 0 : planqui) : 0 ) )
       if( load )
        setLoad(false)
 
   }, [price, ncitas, pteSupe, pteInfe, planqui])

   AsyncStorage.getItem("user").then(val => {
        if( route.params.bugID !=  key )
            setkey( route.params.bugID)
   }).catch(err => console.log(err))

   useEffect(() => {
    if(company && user) {
        if( route.params.action == "Ver" && key > 0 ) {
            getBuge.get(`company=${company}&userID=${route.params.user.id}&doctID=${user}&bugetID=${key}&type=p`).then(rpt => {
                if (rpt.status) {                   
                    setNCitas(rpt.data[0].ncitas.toString())
                    setFCitas(rpt.data[0].fcitas.toString())
                    setPrice(parseInt(rpt.data[0].price).toString())
                    if(parseInt(rpt.data[0].blanqui) > 0)
                     setPlanqui(parseInt(rpt.data[0].blanqui).toString())
                    if(parseInt(rpt.data[0].rteSupe) > 0)
                    setPteSupe(parseInt(rpt.data[0].rteSupe).toString())
                    if(parseInt(rpt.data[0].rteInfe) > 0)
                    setPteInfe(parseInt(rpt.data[0].rteInfe).toString())
                    setState(parseInt(rpt.data[0].state))
                    setToken(rpt.data[0].fcmToken.toString())
                    setRteInfe(parseInt(rpt.data[0].rteInfe) > 0 ? true : false)
                    setRteSupe(parseInt(rpt.data[0].rteSupe) > 0 ? true : false)
                    setBlanqui(parseInt(rpt.data[0].blanqui) > 0 ? true : false)
                    setPhotos(rpt.data[0].photos)
                    setDiagnos(rpt.data[0].note)
                }
                else
                  msgNoti(rpt.data)
            })
        } else {
            cleanData()
            setToken(route.params.user.fcmToken)
        }            
    }
   }, [key])

   const handleToUpdate = (someArg) => {
        setFCitas(someArg);
   }

   const getByIDBugetfunc = (callback) => {
        if(company && user) {
            if( route.params.action == "Ver" && key > 0 ) {
                getBuge.get(`company=${company}&userID=${route.params.user.id}&doctID=${user}&bugetID=${key}&type=p`).then(rpt => {
                    if (rpt.status) {
                        console.log( rpt );
                        setNCitas(rpt.data[0].ncitas.toString())
                        setFCitas(rpt.data[0].fcitas.toString())
                        setPrice(parseInt(rpt.data[0].price).toString())
                        if(parseInt(rpt.data[0].blanqui) > 0)
                        setPlanqui(parseInt(rpt.data[0].blanqui).toString())
                        if(parseInt(rpt.data[0].rteSupe) > 0)
                        setPteSupe(parseInt(rpt.data[0].rteSupe).toString())
                        if(parseInt(rpt.data[0].rteInfe) > 0)
                        setPteInfe(parseInt(rpt.data[0].rteInfe).toString())
                        setState(parseInt(rpt.data[0].state))
                        setToken(rpt.data[0].fcmToken.toString())
                        setRteInfe(parseInt(rpt.data[0].rteInfe) > 0 ? true : false)
                        setRteSupe(parseInt(rpt.data[0].rteSupe) > 0 ? true : false)
                        setBlanqui(parseInt(rpt.data[0].blanqui) > 0 ? true : false)
                        setPhotos(rpt.data[0].photos)
                        setDiagnos(rpt.data[0].note)
                    }
                    else
                     msgNoti(rpt.data)
                })
            } else {
                cleanData()
                setToken(route.params.user.fcmToken)
            }
        }
   }

   const Notification = (token,body,callback) => {
      sendNotification.get(`company=${company}&token=${token}&body=${body}`).then(rpt => {
        callback( rpt );
      })
   }

   const cleanData = () => {
        setNCitas("0")
        setTotal(0)
        setFCitas("8")
        setRteInfe(false)
        setRteSupe(false)
        setBlanqui(false)
   }

   const constformatNumber = ( num, fixed ) => { 
        var decimalPart;

        var array = Math.floor(num).toString().split('');
        var index = -3; 
        while ( array.length + index > 0 ) { 
            array.splice( index, 0, '.' );              
            index -= 4;
        }

        if(fixed > 0){
            decimalPart = num.toFixed(fixed).split(".")[1];
            return array.join('') + "," + decimalPart; 
        }
        return array.join(''); 
   }

   const sheetRef = React.useRef(null);
   const fall = new Animated.Value(1)
   const isCarousel = React.useRef(null)

   const renderInner = () => (
    <View style={styles.panel}>
      <View style={{alignItems: 'center', marginTop: -25}}>
        <Text style={styles.panelTitle}>Diagnóstico del paciente</Text>
        <Text 
            style={styles.panelSubtitle}
            onPress={() => {
                if( photos != "" && photos != null )
                {
                    let dta = JSON.parse(photos)
                    dta.forEach(async(element, ind) => {
                        element.imgUrl = `${server}/ppto/${company}/${route.params.user.title}/${key}/${element.imgUrl}`
                    });
                    setPhto(dta)
                    setVisible(true)
                } else
                  msgNoti("No has cargo fotografías")
            }}
        >
            Ver Galeria Fotos
        </Text>
        <Input
            title=""
            value={diagnos}
            custom={{
                onChangeText: val => setDiagnos(val),
                multiline: true,
                style: {
                    borderWidth: 1,
                    borderColor: "gray",
                    width: '100%',
                    color: "#000",
                    borderRadius: 5,
                    height: 80
                }
            }}
        />
      </View>

      <TouchableOpacity 
            style={styles.panelButton} 
            onPress={async () => 
            {
               let rp = await requestCameraPermission();
               if(rp) 
               {
                    let options = {
                        mediaType: 'photo', cameraType: 'front',
                        maxWidth: 300, maxHeight: 300, quality: 0.9,
                    }

                    launchCamera(options, (res) => {
                        if(!res.didCancel)
                        {
                            let body = {
                                company: company, doctID: user,
                                userName: route.params.user.title,
                                userID: route.params.user.id,
                                bugetID: key, note: diagnos
                            }

                            uploadPpto(res, body).then(other => {
                                if( other.data ) 
                                {
                                    let img = []
                                    if( photos != null ) {
                                        img = JSON.parse(photos)
                                    }

                                    img.push({ title: (img.length + 1), body: "", imgUrl: other.photo })
                                    body.photos = JSON.stringify(img)

                                    getSaveBuget.post( body ).then(rpt => {
                                        if( rpt.status ) {
                                            getByIDBugetfunc(function(){ })
                                        }
                                    }).catch(err => {
                                        console.log(err)
                                    })
                                }
                            })
                        }
                    });
                }
           }}
        >
        <Text style={styles.panelButtonTitle}>Tomar foto</Text>
      </TouchableOpacity>
      
      <TouchableOpacity 
            style={styles.panelButton} 
            onPress={async () => 
            {
                let rp = await requestCameraPermission();
                if(rp) 
                {
                    let options = {
                        mediaType: 'photo', cameraType: 'front',
                        maxWidth: 300, maxHeight: 300, quality: 0.9,
                    }
                    
                    launchImageLibrary(options, (res) => {
                        if(!res.didCancel)
                        {
                            let body = {
                                company: company, doctID: user,
                                userName: route.params.user.title,
                                userID: route.params.user.id,
                                bugetID: key, note: diagnos
                            }

                            uploadPpto(res, body).then(other => {
                                if( other.data ) 
                                {
                                    let img = []
                                    if( photos != null ) {
                                        img = JSON.parse(photos)
                                    }

                                    img.push({ title: (img.length + 1), body: "", imgUrl: other.photo })
                                    body.photos = JSON.stringify(img)

                                    getSaveBuget.post( body ).then(rpt => {
                                        if( rpt.status ) {
                                            getByIDBugetfunc(function(){ })
                                        }
                                    }).catch(err => {
                                        console.log(err)
                                    })
                                }
                            })
                        }
                    });
                }
            }}
      >
        <Text style={styles.panelButtonTitle}>Seleccionar foto</Text>
      </TouchableOpacity>

      <TouchableOpacity 
          style={styles.panelButton} 
          onPress={() => {
            let body = {
                company: company, doctID: user,
                userName: route.params.user.title,
                userID: route.params.user.id,
                bugetID: key, note: diagnos
            }

            getSaveBuget.post( body ).then(rpt => {
                if( rpt.status ) {
                    getByIDBugetfunc(function() { })
                }
            }).catch(err => {
                console.log(err)
            })
        }}
      >
        <Text style={styles.panelButtonTitle}>Guardar</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => sheetRef.current.snapTo(1)}>
        <Text style={styles.panelButtonTitle}>Cancelar</Text>
      </TouchableOpacity>
    </View>
  );

   const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
   );

   return(
       <View style={styles.container}>

           <BottomSheet
             ref={sheetRef}
             snapPoints={[400, 0]}
             initialSnap={1}
             callbackNode={fall}
             renderContent={renderInner}
             renderHeader={renderHeader}
             enabledGestureInteraction={true}
           />

           <View style={{alignContent: 'flex-end', alignItems: 'flex-end'}} >
               <Text style={{height: 80, marginTop: -25}}>
                    <Image source={icono}/>
               </Text>
           </View>

           <View style={{alignContent: 'center', alignItems: 'center'}} >
               <View>

                <Image style={styles.img} source={{ uri: route.params.user.img }} />

                <Badge
                    status={(token == "" ? "error" : "success")} 
                    value={<Text style={{color: "#FFF"}}>T</Text>}
                    containerStyle={{ position: 'absolute', top: 5, right:5 }}
                />
              </View>
              <Text style={{color:"#FFF", marginTop: 5}}> {route.params.user.title} </Text>
           </View>

           <View
             style={{alignContent: 'center',
                     alignItems: 'center',
                     marginBottom: 15,
                     marginTop: 10,
                     flexDirection: 'row',
                   }}
            >
               <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("Patient")
                  }}
               >
                <MaterialCommunityIcons
                    name="chevron-left"
                    size={40}
                    color="#fff"
                    style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>

               <Text style={{fontSize: 28, color: "#d3df4d", marginLeft: (Dimensions.get('window').width / 2) * 0.25 }} >
                    $ Presupuestos
               </Text>

               <TouchableOpacity
                  style={{position: 'absolute', right: 0}}
                  onPress={() => {
                    setNCitas("0")
                    setFCitas("8")
                    setRteInfe(false)
                    setRteSupe(false)
                    setBlanqui(false)
                  }}
               >
                <MaterialCommunityIcons
                    name="plus"
                    size={40}
                    color="#fff"
                    style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>
           </View>
    
           <ScrollView>
                <View style={{ justifyContent: 'center', 
                                alignItems:'center', 
                                backgroundColor: "#e5e5e6",
                                height: 35,
                    }}>
                    <Text style={{ color: "#2f4f4f", fontSize: 20 }}>
                        Presupuesto Ortodoncia
                    </Text>

                </View>
                {/* # Citas */}
                <View style={{ backgroundColor: "#312b2d",
                                height: 35,
                                marginTop: 7,
                                paddingLeft: 6,
                                flexDirection: 'row',
                    }}>

                
                        <Text style={{ color: "#d3df4d", fontSize: 20, marginRight: 5, marginTop: 3, fontWeight: 'bold' }}>
                            # Citas:
                        </Text>

                        <TextInput
                                style={{ color: "#FFF",
                                        borderColor: "#000",
                                        borderWidth: 1,
                                        fontSize: 20,
                                        padding: 5,
                                        paddingLeft: 14,
                                        width: 60
                                    }}
                                value={ncitas}
                                keyboardType='numeric'
                                editable={true}
                                onChangeText={setNCitas}
                        />

                        <Text style={{ color: "#d3df4d", fontSize: 20, marginLeft:20, marginTop: 3, marginRight: 5, fontWeight: 'bold' }}>
                            Valor citas:
                        </Text>

                        <TextInput
                            style={{ color: "#FFF",
                                borderColor: "#000",
                                borderWidth: 1,
                                fontSize: 20,
                                padding: 5,
                                paddingLeft: 14,
                                width: 110,
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                            }}
                            value={price}
                            keyboardType='numeric'
                            editable={true}
                            onChangeText={setPrice}
                        />
                    
                </View>
                {/* Frecuencia de Citas */}
                <View style={{ backgroundColor: "#312b2d",
                                height: 70,
                                marginTop: 5,
                                paddingLeft: 6,
                                flexDirection: 'column',
                    }}>
                    <Text style={{ color: "#d3df4d", fontSize: 20, marginRight: 5, fontWeight: 'bold' }}>
                        Frecuencia de citas:
                    </Text>

                    <View style={{marginTop: 10}}>
                            <RadioButton PROP = {PROP} handleToUpdate = {handleToUpdate} val = {fcitas} active = {false} />
                    </View>

                </View>
                {/* Retenedor Inferior */}
                <View style={{ backgroundColor: "#312b2d",
                                height: 45,
                                marginTop: 7,
                                paddingLeft: 22,
                                flexDirection: 'row',
                    }}>

                    <CheckBox
                        style={{flex: 1, padding: 10,  transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],}}
                        onClick={()=> {
                            setRteInfe( !rteInfe )
                        }}
                        isChecked={rteInfe}
                        rightText={"Retenedor Inferior:   $"}
                        rightTextStyle={{color: "#FFF", fontSize: 20}}
                        checkBoxColor={"#FFF"}
                        checkedCheckBoxColor={"#d3df4d"}
                        CheckboxRadius={100}
                        disabled={false}
                    />

                    <TextInput
                        style={{ color: "#FFF",
                            borderColor: "#000",
                            borderWidth: 1,
                            fontSize: 20,
                            padding: 5,
                            paddingLeft: 14,
                            width: 110,
                            position: 'absolute',
                            right: 0,
                            bottom: 3,
                        }}
                        value={pteInfe}
                        keyboardType='numeric'
                        editable={true}
                        onChangeText={setPteInfe}
                    />

                </View>
                {/* Retenedor Superior */}
                <View style={{ backgroundColor: "#312b2d",
                                height: 45,
                                marginTop: 7,
                                paddingLeft: 22,
                                flexDirection: 'row',
                    }}>

                        <CheckBox
                            style={{flex: 1, padding: 10,  transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],}}
                            onClick={()=> {
                                setRteSupe( !rteSupe )
                            }}
                            isChecked={rteSupe}
                            rightText={"Retenedor Superior: $"}
                            rightTextStyle={{color: "#FFF", fontSize: 20}}
                            checkBoxColor={"#FFF"}
                            checkedCheckBoxColor={"#d3df4d"}
                            CheckboxRadius={100}
                            disabled={false}
                        />

                        <TextInput
                            style={{ color: "#FFF",
                                borderColor: "#000",
                                borderWidth: 1,
                                fontSize: 20,
                                padding: 5,
                                paddingLeft: 14,
                                width: 110,
                                position: 'absolute',
                                right: 0,
                                bottom: 3,
                            }}
                            value={pteSupe}
                            keyboardType='numeric'
                            editable={true}
                            onChangeText={setPteSupe}
                        />

                </View>
                {/* Gengivoplastia */}
                <View style={{ 
                    backgroundColor: "#312b2d",
                    height: 65,
                    marginTop: 7,
                    paddingLeft: 22,
                    flexDirection: 'row',
                }}>

                    <CheckBox
                        style={{flex:1, padding: 10,  transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }], width: "50%"}}
                        onClick={()=> {
                            setBlanqui( !blanqui )
                        }}
                        isChecked={blanqui}
                        rightText={"Gengivoplastia y \n blanquiamiento:"}
                        rightTextStyle={{color: "#FFF", fontSize: 19, width: "40%"}}
                        checkBoxColor={"#FFF"}
                        checkedCheckBoxColor={"#d3df4d"}
                        CheckboxRadius={100}
                        disabled={false}
                    />

                    <TextInput
                        style={{ color: "#FFF",
                            borderColor: "#000",
                            borderWidth: 1,
                            fontSize: 20,
                            padding: 5,
                            paddingLeft: 14,
                            width: 110,
                            position: 'absolute',
                            right: 0,
                            bottom: 10,
                        }}
                        value={planqui}
                        keyboardType='numeric'
                        editable={true}
                        onChangeText={setPlanqui}
                    />

                </View>
                {/* Total */}
                <View style={{ backgroundColor: "#312b2d",
                                height: 40,
                                marginTop: 7,
                                paddingLeft: 6,
                                flexDirection: 'row',
                    }}>

                
                        <Text style={{ color: "#d3df4d", fontSize: 25, marginRight: 5, fontWeight: 'bold' }}>
                            Valor Total :
                        </Text>

                        <Text style={{fontSize: 25, color: "#FFF", position: 'absolute', right:5}} >
                            ${constformatNumber(parseInt(total),0)}
                        </Text>
                    
                </View>
                {/* Guardar */}
                <View style={{flexDirection: 'row'}}>

                    <View style={{alignSelf: 'flex-start', marginTop: 5}} >
                        <TouchableOpacity
                            style={{backgroundColor:"#d3df4d", width:120, height:40, padding:6, borderRadius:10}}
                            onPress={() => {
                                if(key > 0)
                                  sheetRef.current.snapTo(0)
                                else
                                  msgNoti("Debes guardar un presupuesto primero")
                            }}
                        >
                            <Text style={{fontSize: 20, color: "#000", fontWeight: 'bold'}} >
                              Diagnostico
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{position: 'absolute', right:0, marginTop: 5}} >
                        <TouchableOpacity
                            style={{backgroundColor:"#d3df4d", width:90, height:40, padding:6, borderRadius:10}}
                            onPress={() => {
                                if( ncitas != "0") {
                                    const bugt = {
                                        price, ncitas, total, fcitas, company,
                                        rteInfe: ( rteInfe ? parseInt(pteInfe) : 0 ), 
                                        rteSupe: (rteSupe ? parseInt(pteSupe) : 0), 
                                        blanqui: (blanqui ? parseInt(planqui) : 0),
                                        doctID: user, userID: route.params.user.id,
                                        bugetID: key < 0 ? 0 : key
                                    }

                                    buget.post( bugt ).then( rpt => {
                                        if (!state && token != "") {
                                            let msg = `Tiene un presupuesto pendiente por aprobar`
                                            Notification(token,msg,function(){})
                                        }
                                        msgNoti( rpt.data )
                                        navigation.goBack()
                                    }).catch( err => {
                                        console.log(err);
                                    })
                                } else
                                    msgNoti( "Ingrese número de citas" )
                            }}
                        >
                            <Text style={{fontSize: 20, color: "#000", fontWeight: 'bold'}} >
                                Guardar
                            </Text>
                        </TouchableOpacity>
                    </View>

                </View>
        </ScrollView>

        <Modal
            visible={visible}
            onClose={() => {
                setVisible(false)
            }}
            onShow={{}}
        >
            <View>
                <Text style={{color: "#FFF"}}>
                    <Carousel
                        layout="default"
                        layoutCardOffset={9}
                        ref={isCarousel}
                        data={phto}
                        renderItem={CarouselCardItem}
                        sliderWidth={SLIDER_WIDTH}
                        itemWidth={ITEM_WIDTH}
                        inactiveSlideShift={0}
                        useScrollView={true}
                    />
                </Text>
            </View>

        </Modal>

       </View>
   )
}
export default addPpto