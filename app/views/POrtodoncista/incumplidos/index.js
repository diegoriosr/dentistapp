import React, { useLayoutEffect, useState } from 'react'
import { View, Text, Image, StyleSheet, ScrollView, Dimensions } from 'react-native'
import { TouchableOpacity} from 'react-native-gesture-handler'
import { useFocusEffect } from '@react-navigation/native'
import TextResize from "rn-autoresize-font"
import { List } from 'react-native-paper'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import DatePicker from 'react-native-date-picker'
import Carousel from 'react-native-snap-carousel'
import { useSelector } from 'react-redux'
import {launchCamera} from 'react-native-image-picker'
//assets
import icono from '../../../assets/icons/logo.png'
import { uploadFiles } from '../../../utils/uploadPhotos'
//
import {
    getNewAppoint,
    getAllIncumplidos,
    getAprovAppoint,
    getSaveAppoint,
    sendNotification,
    server,
    msgNoti
} from '../../../settings/global'
//components
import Modal from '../../../components/modal'
import Input from '../../../components/input'
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from '../../../components/carusel'

const incumplidos = (props) => {

    const navigation = props.navigation

    const company = useSelector((state) => state.user.company)
    const doctID = useSelector((state) => state.user.id)

    const isCarousel = React.useRef(null)
    
    const [data, setData] = useState([])
    const [expanded, setExpanded] = useState(true)
    const [date, setDate] = useState(new Date())
    const [visible, setVisible] = useState(false)
    const [phto, setPhto] = useState([])

    const [notas, setNotas] = useState()

    const handlePress = () => setExpanded(!expanded)

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "",
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: '#d3df4d',
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerShown: true,
            headerRight: () => {
              return (
                <View style={{flexDirection: 'row'}}>
                  <Image
                      source={icono}
                      style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                  />
                </View>
              )
           },
        })
 
    }, [navigation])

    const getAllAppointfunc = (callback) => {
        getAllIncumplidos.get(`company=${company}&doctID=${doctID}`).then(rpt => {
            if (rpt.status) {
                callback( rpt.data );
            } else {
                msgNoti(rpt.data)
                callback([])
            }
        })
    }

    const Notification = (token, body, callback) => {
        sendNotification.get(`company=${company}&token=${token}&body=${body}&typ=cita&usr=doctor`).then(rpt => {
            callback( rpt );
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            setVisible(false)
            getAllAppointfunc(function (req) {
                if(req.length > 0)
                    setData(req)
                else {
                    setData([])
                    msgNoti("No tienes pacientes incumplidos")
                }
            })
        }, [])
    );

    return( 
        <View style={styles.container}>

           <View style={{alignSelf: 'center', marginTop: 10, flexDirection: 'row', }}>
               <Text style={{fontSize: 26, color: "#d3df4d"}}>
                    Pacientes Incumplidos
               </Text>
           </View>

           <ScrollView>
           <View style={{width: '100%', flex: 1}}>
                <List.Section title="." titleStyle={{color: "#000", fontSize: 1}}>
                   {
                        data.map((item, i) => (
                            <List.Accordion
                              style={{backgroundColor: "#000"}}
                              key={i}
                              title={ "Cita " + item.order + " de " + item.ncitas }
                              titleStyle={{color: (item.status == 4 ? "#00FDFF" : "#D9DA08"), fontSize: 20 }}
                              descriptionStyle={{color: "#FFF", fontSize: 15, padding: 1}}
                              description={ 
                                     item.userName + " " + item.lastName +" \n" +
                                     item.approvedDate.slice(0,10) + " " + new Date(item.approvedDate).toLocaleTimeString().slice(0,5) 
                              }
                              onPress={handlePress}
                              //expanded={expanded}
                              right={props => <List.Icon  {...props} icon={"chevron-" + (expanded ? "up" : "down")} color="#D9DA08" />}
                              left={props => <List.Icon  {...props} icon="calendar" color={(item.status == 4 ? "#00FDFF" : "#D9DA08")} />}>
                              <List.Item 
                                 title={(item.status == 4 ? "Resumen de la cita" : "Seleccionar fecha para la cita")}
                                 titleStyle={{color: "#D9DA08", marginBottom: 15, marginTop: -20, fontSize: 20, alignSelf: 'center' }}
                                 style={{backgroundColor: "#000", fontSize: 20, marginLeft: -40}}
                                 descriptionStyle={{flex: 1,width:'100%', marginLeft: -10}}
                                 description={
                                    item.status != 4 ?
                                    <>
                                        <DatePicker
                                            date={(item.approvedDate == undefined ? date : new Date(item.approvedDate))}
                                            locale='es-CO'
                                            textColor={"#D9DA08"}
                                            fadeToColor={"#000"}
                                            onDateChange={setDate}
                                        />

                                        <View style={{marginTop: 10, width: '100%'}}>

                                            <Text style={{color: "#FFF", fontSize: 16}}>
                                               Estado: {(item.status == 0 ? "Sin enviar" : ( item.status == 1 ? "Enviado" : ( item.status == 2 ? "El paciente no ha asistido" : "Cita realizada" ) ) )}
                                            </Text>

                                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                            
                                                <TouchableOpacity
                                                    disabled={(item.status == 2 ? false : true )}
                                                    style={styles.buttonRe} 
                                                    onPress={() => 
                                                    {
                                                        let dt = [{
                                                            company: item.company, appointID: item.appointID,
                                                            bugetID: item.bugetID, userID: item.userID,
                                                            doctID: item.doctID, status: 1,
                                                            order: item.order, approvedDate: date,
                                                            reschedule: 1, note: null, photos: null
                                                        }]

                                                        getNewAppoint.post( dt ).then(rpt => {
                                                            if( rpt.status ) {
                                                                let msg = `Se reprogramo su cita para el día ${date}`
                                                                msgNoti("La cita se reprogramó con exito")
                                                                Notification(item.fcmToken,msg,function(){})
                                                                getAllAppointfunc(function(req) {
                                                                    setData(req)
                                                                })
                                                            }
                                                        }).catch(err => {
                                                            console.log(err);
                                                        })
                                                    }}
                                                >
                                                    <Text style={styles.title}> {(item.status == 0 ? "Enviar" : ( item.status == 1 ? "Enviado" : ( item.status == 2 ? " Reprogramar " : "Realizada" ) ))} </Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity 
                                                    style={styles.buttonIn}
                                                    onPress={() => {
                                                        let pst = {
                                                            userID: item.userID, company,
                                                            doctID: item.doctID,
                                                            bugetID:item.bugetID,
                                                            appointID: item.appointID,
                                                            status: 4
                                                        }
        
                                                        getAprovAppoint.post( pst ).then(rpt => {
                                                        if( rpt.status ) {
                                                            getAllAppointfunc(function(req){
                                                                setData(req)
                                                            })
                                                        }
                                                        }).catch(err => {
                                                            console.log(err);
                                                        })
                                                    }}
                                                >
                                                    <Text style={styles.title}>
                                                    &nbsp; Iniciar &nbsp;
                                                    </Text>
                                                </TouchableOpacity>

                                            </View>    
                                        </View>
                                    </>
                                    :
                                    <>
                                        <View>                            
                                            <View style={{flexDirection: 'row', alignSelf: 'center'}}>

                                                <TouchableOpacity
                                                    onPress={() => {
                                                        let options = {maxWidth:300, maxHeight:300, quality:1, cameraType:'front' }
                                                        launchCamera(options, (res) => {
                                                            if(!res.didCancel) {
                                                            
                                                                let body = {
                                                                    company: item.company, doctID: item.doctID,
                                                                    userName: item.userName+" "+item.lastName,
                                                                    userID: item.userID,
                                                                    bugetID: item.bugetID, appointID: item.appointID
                                                                }
                                                                
                                                                uploadFiles(res, body).then(other => 
                                                                {
                                                                    if( other.data ) 
                                                                    {
                                                                        let img = []

                                                                        if( item.photos != null ) { 
                                                                            img = JSON.parse(item.photos)
                                                                        }
                                                                        img.push({ title: (img.length + 1), body: "", imgUrl: other.photo })

                                                                        body.photos = JSON.stringify(img)

                                                                        getSaveAppoint.post( body ).then(rpt => {
                                                                            if( rpt.status ) {
                                                                                getAllAppointfunc(function(req) {
                                                                                    setData(req)
                                                                                })
                                                                            }
                                                                        }).catch(err => {
                                                                            console.log(err)
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }}
                                                >
                                                    <MaterialCommunityIcons
                                                        name="camera"
                                                        size={40}
                                                        color="#fff"
                                                        style={{color: "#d3df4d"}}
                                                    />
                                                </TouchableOpacity>

                                                <TouchableOpacity
                                                    onPress={() => {
                                                        let dta = JSON.parse(item.photos)
                                                        dta.forEach(async(element, ind) => {
                                                            element.imgUrl = `${server}/citas/${company}/${item.userName+" "+item.lastName}/${item.bugetID}/${item.appointID}/${element.imgUrl}`
                                                        });
                                                        setPhto(dta)
                                                        setVisible(true)
                                                    }}
                                                >
                                                    <MaterialCommunityIcons
                                                        name="image"
                                                        size={40}
                                                        color="#fff"
                                                        style={{color: "#d3df4d"}}
                                                    />
                                                </TouchableOpacity>
                                            
                                                <TouchableOpacity
                                                    onPress={() => 
                                                    {
                                                        let pst = {
                                                            userID: item.userID, company,
                                                            doctID: item.doctID,
                                                            bugetID:item.bugetID,
                                                            appointID: item.appointID,
                                                            status: 3
                                                        }
            
                                                        getAprovAppoint.post( pst ).then(rpt => {
                                                            if( rpt.status ) {
                                                                getAllAppointfunc(function(req){
                                                                    setData(req)
                                                                })
                                                            }
                                                        }).catch(err => {
                                                            console.log(err);
                                                        })
                                                    }}
                                                >
                                                    <MaterialCommunityIcons
                                                        name="checkbox-marked-outline"
                                                        size={40}
                                                        color="#fff"
                                                        style={{color: "#d3df4d"}}
                                                    />
                                                </TouchableOpacity>
                                            
                                            </View>

                                            <View style={{height: 170, width: Dimensions.get('window').width - 40, marginBottom: 5}}>
                                                <TextResize 
                                                  text={item.note}
                                                  numberOfLines={8}
                                                  style={{ color: "#FFF", textAlign: "center", width: "100%", marginTop: 10 }}
                                                  fontSize={15}
                                               />

                                               <Input
                                                title="Notas de la cita"
                                                value={notas}
                                                custom={{
                                                    onChangeText: val => setNotas(val),
                                                    multiline: true,
                                                    style: {
                                                        borderWidth: 1,
                                                        borderColor: "#d3df4d",
                                                        width: "100%",
                                                        paddingHorizontal: 10,
                                                        color: "#FFF",
                                                        borderRadius: 5,
                                                        height: 100
                                                    }
                                                }}
                                              />
                                            </View>

                                            <TouchableOpacity
                                                style={styles.button} 
                                                onPress={() => {
                                                    let body = {
                                                        company: item.company, doctID: item.doctID,
                                                        userID: item.userID, bugetID: item.bugetID, 
                                                        appointID: item.appointID, note: notas
                                                    }

                                                    getSaveAppoint.post( body ).then(rpt => {
                                                        if( rpt.status ) {
                                                            setNotas("")
                                                            getAllAppointfunc(function(req) {
                                                                setData(req)
                                                            })
                                                        }
                                                    }).catch(err => {
                                                        console.log(err)
                                                    })
                                                }}
                                            >
                                                <Text style={styles.title}> Guardar </Text>
                                            </TouchableOpacity>
                                        
                                        </View>
                                    </>
                                 }
                              />
                            </List.Accordion>
                        ))
                   }
                </List.Section>
           </View>
           </ScrollView>

           <Modal
                visible={visible}
                onClose={() => {
                    setVisible(false)
                }}
                onShow={{}}
            >
                <View>
                    <Text style={{color: "#FFF"}}>
                       <Carousel
                            layout="default"
                            layoutCardOffset={9}
                            ref={isCarousel}
                            data={phto}
                            renderItem={CarouselCardItem}
                            sliderWidth={SLIDER_WIDTH}
                            itemWidth={ITEM_WIDTH}
                            inactiveSlideShift={0}
                            useScrollView={true}
                        />
                    </Text>
                </View>

            </Modal>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
        width: '100%'
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    },
    button: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "70%",
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 5,
        marginEnd: 10
    },
    buttonRe: {
        position: 'relative',
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "95%",
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginEnd: 10
    },
    buttonIn: {
        position: 'relative',
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "95%",
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginEnd: 40
    },
    title: {
        color: "#d3df4d",
        fontSize: 16,
        fontWeight: "bold",
        marginVertical: 8,
    },
})

export default incumplidos;