import React, { useLayoutEffect, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { View, Text, Image, StyleSheet, FlatList } from 'react-native'
import { ListItem, Avatar, Badge } from "react-native-elements"
import { useSelector } from 'react-redux'

//assets
import icono from '../../../assets/icons/logo.png'
import {getAllApprove, msgNoti} from '../../../settings/global'

const miCitas = (props) => {

    const navigation = props.navigation

    const user = useSelector((state) => state.user)    
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Agendamiento Citas",
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: "#FFF",
            headerTitleStyle: {
                color: "#1f1f1f"
            },
            headerShown: true,
            headerRight: () => {
             return (
               <View style={{flexDirection: 'row'}}>
                 <Image
                     source={icono}
                     style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                 />
               </View>
             )
          },
        })
 
    }, [navigation])

    const viewBuget = async(callback) => {
        let data = []
        getAllApprove.get(`company=${user.company}&doctID=${user.id}`).then(rpt => {
            if (rpt.status) {
                if(rpt.data.length) {
                    rpt.data.forEach(async (val, ind) => {                      
                        data.push( val )
                        if( ind === rpt.data.length -1 ) {
                            callback(data)
                        }
                    })
                }
                else
                    callback(false)
            }
            else
                msgNoti(rpt.data)
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            viewBuget(function(rpt) {
                if(rpt) {
                  setData(rpt)
                }
                else {
                  setData([])
                  msgNoti("Tus pacientes no han aprobado\n presupuestos / Citas")
                }
            })
        }, [])
    );

    return( 
        <View style={styles.container}>

            <View style={styles.subnontainer}> 

                <Text style={{fontSize: 26, color: "#d3df4d"}}> 
                   Citas programadas
                </Text>

            </View>

             <FlatList 
                data={data}
                keyExtractor={item => item.bugetID}
                renderItem={({ item }) => (
                    <ListItem bottomDivider 
                              containerStyle={{backgroundColor:"rgba(1,1,1, 0.7)"}}
                              onPress={() => {
                                  props.navigation.navigate("CitasxPpte",  { 
                                    buget: item.bugetID, 
                                    fecha: item.approvedDate, 
                                    tCitas: item.ncitas,
                                    fCitas: item.fcitas,
                                    nombre: item.userName +" " + item.lastName,
                                    patient: item.userID,
                                    fcmToken: item.fcmToken
                                  })
                              }}
                    >
                        <View>
                            <Avatar
                                rounded 
                                source={{uri: item.pathURL}} 
                                size="large"
                            />

                            <Badge
                                status={( item.fcmToken == "" ? "error" : "success" )}
                                value={<Text style={{color: "#FFF"}}>T</Text>}
                                containerStyle={{ position: 'absolute', top: 5, right: -4 }}
                            />
                        </View>

                        <ListItem.Content  style={{ height: 20}}>

                           <ListItem.Title style={{color: "#D9DA08" }}> 
                               {item.userName} {item.lastName}
                           </ListItem.Title>

                           <ListItem.Subtitle style={{color: "#FFF" }}>
                               Aprobado: {item.approvedDate}&nbsp;&nbsp; 
                               Total citas: {item.ncitas}
                           </ListItem.Subtitle>
                           
                        </ListItem.Content>

                        <ListItem.Chevron color="#D9DA08" />

                    </ListItem>
                )}
                onRefresh={() => {
                    setLoading(true)
                    viewBuget(function(rpt) {
                        if(rpt) {
                          setData(rpt)
                        }
                        else {
                          msgNoti("Tus pacientes no han aprobado\n presupuestos / Citas")
                          setData([])
                        }
                    })
                    setLoading(false)
                }}
                refreshing={loading}
             />
         </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    subnontainer: {
        marginBottom:20,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: "white"
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    }
})

export default miCitas;