import React, { useLayoutEffect, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { View, Text, Image, StyleSheet, FlatList } from 'react-native'
import { ListItem, SearchBar, Icon } from "react-native-elements"
import { useSelector } from 'react-redux'
//assets
import icono from '../../../assets/icons/logo.png'
//
import { msgNoti} from '../../../settings/global'
import getBuge from '../../../api/buget/getByID'

const bugets = ({route, navigation}) => {

    const lUser = useSelector((state) => state.user)
    
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [temp, setTemp] = useState([])
    const [search, setSearch] = useState(null)
    const [user, setUser] = useState(lUser.id)
    const [company, setCompany] = useState(lUser.company)

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Presupuestos / Citas",
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: "#FFF",
            headerTitleStyle: {
                color: "#1f1f1f"
            },
            headerShown: true,
            headerRight: () => {
             return (
               <View style={{flexDirection: 'row'}}>
                 <Image
                     source={icono}
                     style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                 />
               </View>
             )
          },
        })
 
    }, [navigation])

    const viewBuget = async(callback) => {
        let data = []
        getBuge.get(`company=${company}&userID=${user}`).then(rpt => {
            if (rpt.status) {
                if(rpt.data.length) {
                    rpt.data.forEach(async (val, ind) => {                      
                        data.push( val )
                        if( ind === rpt.data.length -1 ) {
                            callback(data)
                        }
                    })
                }
                else
                    callback(false)
            }
            else
             msgNoti(rpt.data)
        })
    }

    const constformatNumber = ( num, fixed ) => { 
        var decimalPart;
    
        var array = Math.floor(num).toString().split('');
        var index = -3; 
        while ( array.length + index > 0 ) { 
            array.splice( index, 0, '.' );              
            index -= 4;
        }
    
        if(fixed > 0){
            decimalPart = num.toFixed(fixed).split(".")[1];
            return array.join('') + "," + decimalPart; 
        }
        return array.join(''); 
    }

    useFocusEffect(
        React.useCallback(() => {
            viewBuget(function(rpt) {
                if(rpt) {
                  setData(rpt)
                }
            })
        }, [])
    );

    return( 
        <View style={styles.container}>

            <View style={styles.subnontainer}> 

                <Text style={{fontSize: 28, color: "#d3df4d"}}> 
                   $ Presupuestos
                </Text>

            </View>

             <FlatList 
                data={data}
                keyExtractor={item => item.bugetID}
                renderItem={({ item }) => (
                    <ListItem 
                        bottomDivider 
                        containerStyle={{backgroundColor:"rgba(1,1,1, 0.7)"}}
                        onPress={() => {
                            if( !item.state )
                            navigation.navigate("BugetPatient",{ buget: item.bugetID, doctor: item.doctID })
                            else
                            navigation.navigate("MisCitas",{ buget: item.bugetID, doctor: item.doctID })
                        }}
                    >
                        <Icon name={item.icon} type='font-awesome' />

                        <ListItem.Content  style={{ height: 40}}>

                           <ListItem.Title style={{color: "#D9DA08", fontSize:16.5 }}> 
                               #{item.bugetID}  (F. Citas: {item.fcitas} días) - (# Citas: {item.ncitas}) 
                           </ListItem.Title>

                           <ListItem.Subtitle style={{color: "#FFF", fontSize: 15 }}>
                               Fecha: {item.created}&nbsp;&nbsp; 
                               Total: {constformatNumber(parseInt(item.rteInfe) + parseInt(item.rteSupe) + parseInt(item.blanqui) + ( parseInt(item.ncitas) * parseInt(item.price)),0)} 
                           </ListItem.Subtitle>

                           <ListItem.Title style={{color: "#D9DA08", fontSize:15 }}> 
                               Estado: {( item.state == 1 ? "Verificación de citas" : "Pendiente aprobación" )}
                           </ListItem.Title>
                           
                        </ListItem.Content>

                        <ListItem.Chevron color="#D9DA08" />

                    </ListItem>
                )}
                onRefresh={() => {
                    setLoading(true)
                    viewBuget(function(rpt) {
                        if(rpt) {
                          setData(rpt)
                        }
                    })
                    setLoading(false)
                }}
                refreshing={loading}
             />
         </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    subnontainer: {
        marginBottom:20,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: "white"
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    }
})

export default bugets;