import React, { useLayoutEffect, useState  } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { View, Text, Image, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import { ListItem, Icon } from "react-native-elements"
import { useSelector } from 'react-redux'
import PushNotification from 'react-native-push-notification'
//assets
import icono from '../../../assets/icons/logo.png'
import {getAprovAppoint, getBugetNextDate, sendNotification, msgNoti, getNotiArrive} from '../../../settings/global'
import QRCodeScanner from 'react-native-qrcode-scanner';

const bugets = ({navigation}) => {

    const lUser = useSelector((state) => state.user)
    
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [user, setUser] = useState(lUser.id)
    const [company, setCompany] = useState(lUser.company)

    const [scan, setScan] = useState(false)
    const [result, setResult] = useState(null)
    const [scanner , setScanner ] = useState(null)

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Mis Citas",
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: "#FFF",
            headerTitleStyle: {
                color: "#1f1f1f"
            },
            headerShown: true,
            headerRight: () => {
             return (
               <View style={{flexDirection: 'row'}}>
                 <Image
                     source={icono}
                     style={{ width: 110, height: 50, tintColor: '#FFF', marginRight: 10 }}
                 />
               </View>
             )
          },
        })
 
    }, [navigation])

    const viewBuget = async(callback) => {
        let data = []
        getBugetNextDate.get(`company=${company}&userID=${user}`).then(rpt => {
            if (rpt.status) {
                if(rpt.data.length) {
                    rpt.data.forEach(async (val, ind) => {
                        data.push( val )
                        if( ind === rpt.data.length -1 ) {                  
                            callback(data)
                        }
                    })
                }
                else
                    callback(false)
            }
            else {
                msgNoti(rpt.data)
                callback(false)
            }
        })
    }

    const EmptyListMessage = ({item}) => {
        return (
          // Flat List Item
          <Text
            style={styles.emptyListStyle}
            onPress={() => {
                setLoading(true)
                viewBuget(function(rpt) {
                    if(rpt) {
                      setData(rpt)
                    }
                })
                setLoading(false)
            }}
          >
               No tiene citas para el día hoy
          </Text>
        );
    };

    const Notification = (token,body,callback) => {
        sendNotification.get(`company=${company}&token=${token}&body=${body}`).then(rpt => {
            callback( rpt );
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            viewBuget(function(rpt) {
                if(rpt) {
                  setData(rpt)
                }
                else
                 setData([])
            })
        }, [])
    );

    const activeQR = () => {
        setScan( scanner ? false : true)
    }

    const onSuccess = (e) => 
    {
        const check = e.data

        if( JSON.parse(check).Company == company &&  parseInt(JSON.parse(check).DoctID) == parseInt(data[0].doctID) ) {

            let pst = {
                userID: data[0].userID, company,
                doctID:data[0].doctID,
                bugetID:data[0].bugetID,
                appointID: data[0].order,
                room: 1
            }

            getNotiArrive.post( pst ).then(rpt => {
                console.log( rpt );
                if( rpt.status ) {
                    viewBuget(function(rpt) 
                    {
                        let msg = `El paciente ${lUser.name} esta en la sala de espera`
                        Notification(data[0].fcmToken,msg,function(){})

                        if(rpt) {
                            setData(rpt)
                        }
                        else
                            setData([])
                    })
                }
            }).catch(err => {
                console.log(err);
            })
        }
        else
         msgNoti("No tiene una cita en este laboratorio o con este doctor")

        setScan(false)
        setResult(e)
    }

    return( 
        <View style={styles.container}>

            {!scan &&
            <>
                <View style={styles.subnontainer}> 

                    <Text style={{fontSize: 22, color: "#d3df4d"}}> 
                        Agenda
                    </Text>

                </View>

                <View style={styles.subnontainer}> 

                    <Image  style={styles.img} source={{uri: lUser.uri}} />

                </View>

                <FlatList 
                    data={data}
                    keyExtractor={item => item.bugetID}
                    renderItem={({ item }) => (
                        <ListItem bottomDivider 
                                containerStyle={{backgroundColor:"rgba(1,1,1, 0.7)", padding:5}}
                        >
                            <Icon name={item.icon} type='font-awesome' />
                            
                            <ListItem.Content  style={{ height: 240}}>

                            <ListItem.Title style={{color: "#D9DA08", fontSize:25, marginLeft: '10%' }}> 
                                Tú próxima cita es para 
                            </ListItem.Title>

                            <ListItem.Subtitle style={{color: "#FFF", fontSize: 30}}>
                                {item.appDateTime}
                            </ListItem.Subtitle>

                            <ListItem.Subtitle style={{color: "#D9DA08", fontSize:25, marginLeft: '27%', marginBottom: 25}}>
                                Cita {item.order} de { item.ncitas}
                            </ListItem.Subtitle>

                            <ListItem.Title style={{color: "#D9DA08", fontSize:20, marginLeft: '25%'}}>
                            {(  item.room == 0 && item.status == 1 ? 
                                    <TouchableOpacity
                                        style={{backgroundColor:'#d3df4d', width:150, height:50, borderRadius:15, alignItems:'center'}}
                                        onPress={() => 
                                        {
                                            if(item.status == 1)
                                            {
                                                let pst = {
                                                    userID: item.userID, company,
                                                    doctID:item.doctID,
                                                    bugetID:item.bugetID,
                                                    appointID: item.order,
                                                    status: 2
                                                }
                                                getAprovAppoint.post( pst ).then(rpt => {
                                                    if( rpt.status ) {
                                                        viewBuget(function(rpt) 
                                                        {
                                                            let msg = `El paciente ha aprobado la cita`
                                                            Notification(item.fcmToken,msg,function(){})

                                                            PushNotification.localNotificationSchedule({
                                                                channelId: 'LocalNoti',
                                                                title: `Recordatorio - DentistPoocket App`,
                                                                message: "Cita programada",
                                                                date: new Date(Date.now() + (43200 * 1000)),
                                                                allowWhileIdle: true
                                                            })

                                                            PushNotification.localNotificationSchedule({
                                                                channelId: 'LocalNoti',
                                                                title: `Recordatorio - DentistPoocket App`,
                                                                message: "Cita programada",
                                                                date: new Date(Date.now() + (86400 * 1000)),
                                                                allowWhileIdle: true
                                                            })

                                                            if(rpt) {
                                                                setData(rpt)
                                                            }
                                                            else
                                                                setData([])
                                                        })
                                                    }
                                                }).catch(err => {
                                                    console.log(err);
                                                })
                                            }
                                        }}
                                    >
                                        <Text 
                                            style={{ color:"#000", fontWeight: 'bold', marginTop: 15, fontSize: 16 }}>
                                            Confirmar
                                        </Text>
                                   </TouchableOpacity>
                                :
                                    <TouchableOpacity
                                        disabled={(item.room == 1 ? true : false )}
                                        style={{backgroundColor:'#d3df4d', width:150, height:50, borderRadius:15, alignItems:'center'}}
                                        onPress={activeQR}
                                    >
                                        <Text 
                                            style={{ color:"#000", fontWeight: 'bold', marginTop: 15, fontSize: 16 }}>
                                            {item.room == 0 ? "Anunciar Llegada" : "Confirmado"}
                                        </Text>
                                    </TouchableOpacity>
                                )}
                            
                            </ListItem.Title>
                            
                            </ListItem.Content>

                            <ListItem.Chevron color="#D9DA08" />

                        </ListItem>
                    )}
                    onRefresh={() => {
                        setLoading(true)
                        viewBuget(function(rpt) {
                            if(rpt) {
                            setData(rpt)
                            } else {
                            setData([])
                            }
                        })
                        setLoading(false)
                    }}
                    refreshing={loading}
                    ListEmptyComponent={EmptyListMessage}
                />
            </>
            }

            {scan &&
                <QRCodeScanner
                    reactivate={true}
                    showMarker={true}                    
                    checkAndroid6Permissions={true}
                    ref={(node) => { setScanner(node) }}
                    onRead={onSuccess}
                />
            }
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    subnontainer: {
        marginBottom:25,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: "white"
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    },
    img: {
        width: 150,
        height: 150,
        borderRadius: 75
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        color:"#d3df4d",
        textAlign: 'center',
    },

    scrollViewStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#99003d'
    },

    scrollViewStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#99003d'
    },
})

export default bugets;