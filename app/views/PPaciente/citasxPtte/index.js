import React, { useLayoutEffect, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import TextResize from "rn-autoresize-font"
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, Dimensions } from 'react-native'
import { List } from 'react-native-paper'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import DatePicker from 'react-native-date-picker'
import Carousel from 'react-native-snap-carousel'
import { useSelector } from 'react-redux'
//import PushNotification from 'react-native-push-notification'
//assets
import icono from '../../../assets/icons/logo.png'
import {getAprovAppoint, getAllAppoint, sendNotification, server, msgNoti} from '../../../settings/global'
//components
import Modal from '../../../components/modal'
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from '../../../components/carusel'

const miCitas = ({route, navigation}) => {

    const company = useSelector((state) => state.user.company)
    const userID = useSelector((state) => state.user.id)

    const isCarousel = React.useRef(null)
    
    const [data, setData] = useState([])
    const [expanded, setExpanded] = useState(false);
    const [date, setDate] = useState(new Date())
    const [visible, setVisible] = useState(false)
    const [phto, setPhto] = useState([])

    const handlePress = () => setExpanded(!expanded);

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Citas Pendientes",
            headerStyle: {
                backgroundColor: "#1f1f1f"
            },
            headerTintColor: "#FFF",
            headerTitleStyle: {
                color: "#1f1f1f"
            },
            headerShown: true,
        })
 
    }, [navigation])

    const getAllAppointfunc = (callback) => {
        getAllAppoint.get(`company=${company}&userID=${userID}&doctID=${route.params.doctor}&bugetID=${route.params.buget}&type=d`).then(rpt => {
            if (rpt.status) {
                callback( rpt.data );
            } else {
                msgNoti(rpt.data)
                callback([])
            }
        })
    }

    const Notification = (token,body,callback) => {
        sendNotification.get(`company=${company}&token=${token}&body=${body}`).then(rpt => {
            callback( rpt );
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            getAllAppointfunc(function (req) {
                if(req.length > 0)
                  setData(req)
                else
                  setData([])
            })
        }, [])
    );

    return(
        <View style={styles.container}>

           <View
             style={{alignContent: 'center', alignItems: 'center', marginTop: -10}}
           >
               <Text style={{height: 68}}>
                    <Image style={styles.img}
                           source={icono}
                    />
               </Text>
           </View>

           <View
             style={{alignContent: 'center',
                     alignItems: 'center',
                     marginBottom: 15,
                     marginTop: 10,
                     flexDirection: 'row',
                   }}
           >
               <TouchableOpacity
                  onPress={() => {
                    navigation.goBack()
                  }}
               >
                <MaterialCommunityIcons
                    name="chevron-left"
                    size={40}
                    color="#fff"
                    style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>

               <Text style={{fontSize: 28, color: "#d3df4d", marginLeft: "15%"}} >
                    Agenda de Citas
               </Text>

               <TouchableOpacity 
                  style={{position: 'absolute', right: 0}}                  
                  onPress={() => {
                    getAllAppointfunc(function (req) {
                        if(req.length > 0)
                         setData(req)
                        else
                        setData([])
                    })
                  }}
               >
                <MaterialCommunityIcons
                    name="calendar-refresh"
                    size={35}
                    color="#fff"
                    style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>
           </View>

           <ScrollView>
           <View>
                <List.Section title="Seguimiento Tratamiento" titleStyle={{color: "#FFF", fontSize: 20}}>
                   {
                        data.map((item, i) => (
                            <List.Accordion
                              style={{backgroundColor: "#000"}}
                              key={i}
                              title={ "Cita " + item.order + " de " + item.ncitas }
                              titleStyle={{color: (item.status == 3 ? "#00FDFF" : "#D9DA08") }}
                              descriptionStyle={{color: "#FFF"}}
                              description={(item.fecha == undefined ? new Date(new Date(item.approvedDate).setSeconds(-18000)).toISOString().slice(0,10) + " " + new Date(new Date(item.approvedDate).setSeconds(-18000)).toISOString().slice(11,16) : item.fecha)}
                              onPress={handlePress}
                              right={props => <List.Icon  {...props} icon={"chevron-" + (expanded ? "up" : "down")} color="#D9DA08" />}
                              left={props => <List.Icon  {...props} icon="calendar" color={(item.status == 3 ? "#00FDFF" : (item.status == 1 ? "#FE0000" : "#D9DA08"))} />}
                              //expanded={item.status == 1 ? true : expanded}
                              >
                              <List.Item
                                 title={(item.status == 3 ? "Resumen de su cita" : "Su cita fue programada para el: ")}
                                 titleStyle={{color: (item.status == 3 ? "#00FDFF" : "#D9DA08"), marginBottom: 15, fontSize: 18, marginTop: -20, alignSelf: 'center' }}
                                 style={{backgroundColor: "#000", fontSize: 20, marginLeft: -40}}
                                 descriptionStyle={{flex: 1, width:'100%', marginLeft: -10}}
                                 description={
                                     item.status != 3 ?
                                        <>
                                        <DatePicker                                          
                                          date={(item.approvedDate == undefined ? date : new Date(item.approvedDate))}
                                          maximumDate={(item.approvedDate == undefined ? date : new Date(item.approvedDate))}
                                          minimumDate={(item.approvedDate == undefined ? date : new Date(item.approvedDate))}
                                          minuteInterval={1}
                                          locale='es-CO'
                                          textColor={"#D9DA08"}
                                          fadeToColor={"#000"}
                                          androidVariant={'nativeAndroid'}
                                          onDateChange={() => { return }}
                                        />
                                        <View style={{marginTop: 10}}>

                                            <Text style={{color: "#FFF"}}> 
                                               Estado: {(item.status == 0 ? "Sin programar" : ( item.status == 1 ? "Programado" : "Cita confirmada" ) )}
                                            </Text>

                                            <TouchableOpacity
                                                disabled={(item.status == 1 ? false : true )}
                                                style={styles.button} 
                                                onPress={() => 
                                                {
                                                    let pst = {
                                                        userID, company,
                                                        doctID:route.params.doctor,
                                                        bugetID:route.params.buget,
                                                        appointID: item.order,
                                                        status: 2
                                                    }

                                                    getAprovAppoint.post( pst ).then(rpt => {
                                                        if( rpt.status ) {
                                                            let msg = `El paciente ha aprobado la cita`
                                                            Notification(item.fcmToken,msg,function(){})
                                                            getAllAppointfunc(function(req){
                                                                setData(req)
                                                            })
                                                        }
                                                    }).catch(err => {
                                                        console.log(err);
                                                    })
                                                }}
                                            >
                                            <Text style={styles.title}> {(item.status == 0 ? "No asignado" : ( item.status == 1 ? "Aprobar" : "Confirmada" ))} </Text>
                                            </TouchableOpacity>
                                        </View>
                                      </>
                                      : 
                                      <>
                                         <View style={{width: Dimensions.get('window').width - 40, marginBottom: 1}}>                                    
                                            
                                            <View style={{flexDirection: 'row'}}>
                                                
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        if( item.photos != null || item.photos != 'null' ) {
                                                            let dta = JSON.parse(item.photos)
                                                            
                                                            dta.forEach(async(element, ind) => {
                                                                element.imgUrl = `${server}/citas/${company}/${item.userName+" "+item.lastName}/${item.bugetID}/${item.appointID}/${element.imgUrl}`
                                                            });
                                                            setPhto(dta)
                                                            setVisible(true)
                                                        }
                                                    }}
                                                >
                                                    <MaterialCommunityIcons
                                                        name="image"
                                                        size={40}
                                                        color="#fff"
                                                        style={{color: "#d3df4d"}}
                                                    />
                                                </TouchableOpacity>
                                            
                                            </View>

                                            <TextResize 
                                                text={item.note}
                                                numberOfLines={8}
                                                style={{ color: "#FFF", textAlign: "center", width: 310, marginTop: 10 }}
                                                fontSize={15}
                                            />
                                        
                                        </View>
                                      </>
                                 }
                              />
                            </List.Accordion>
                        ))
                   }
                </List.Section>
           </View>
           </ScrollView>

           <Modal
                visible={visible}
                onClose={() => {
                    setVisible(false)
                }}
                onShow={{}}
            >
                <View>
                    <Text style={{color: "#FFF"}}>
                       <Carousel
                            layout="default"
                            layoutCardOffset={9}
                            ref={isCarousel}
                            data={phto}
                            renderItem={CarouselCardItem}
                            sliderWidth={SLIDER_WIDTH}
                            itemWidth={ITEM_WIDTH}
                            inactiveSlideShift={0}
                            useScrollView={true}
                        />
                    </Text>
                </View>

            </Modal>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    subnontainer: {
        marginBottom:20,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: "white"
    },
    text: {
        borderColor: "#d3df4d",
        width: "100%",
        color: "#CCC",
    },
    button: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "100%",
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    title: {
        color: "#d3df4d",
        fontSize: 16,
        fontWeight: "bold",
        marginVertical: 8,
    },
})

export default miCitas;