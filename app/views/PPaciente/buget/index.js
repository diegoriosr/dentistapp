import React, { useEffect, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import { View, Text, TextInput, TouchableOpacity, Image, Dimensions } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AsyncStorage from '@react-native-async-storage/async-storage'

//component
import CheckBox from 'react-native-check-box'
import RadioButton from '../../../components/radio'

import getBuge from '../../../api/buget/getBugetID'
import approved from '../../../api/buget/approve'
import {sendNotification, msgNoti} from '../../../settings/global'

//assets
import { styles } from './styles'
import icono from '../../../assets/icons/logo.png'

const checkPpto = ({route, navigation}) => {

   const lUser = useSelector((state) => state.user)

   const [user, setUser] = useState(lUser.id)
   const [company, setCompany] = useState(lUser.company)
   const [key, setkey] = useState(-1)

   const [ncitas, setNCitas] = useState("0")
   const [total, setTotal] = useState(0)
   const [fcitas, setFCitas] = useState("8")
   const [pteInfe, setPteInfe] = useState("170000")
   const [pteSupe, setPteSupe] = useState("170000")
   const [planqui, setPlanqui] = useState("225000")
   const [rteInfe, setRteInfe] = useState(false)
   const [rteSupe, setRteSupe] = useState(false)
   const [blanqui, setBlanqui] = useState(false)
   const [state, setState] = useState(0)
   const [token, setToken] = useState()

   const PROP = [
        {
            key: '8',
            text: '8 d',
        },
        {
            key: '15',
            text: '15 d',
        },
        {
            key: '20',
            text: '20 d',
        },
        {
            key: '30',
            text: '30 d',
    },
   ];

   AsyncStorage.getItem("user").then(val => {
    if( route.params.buget !=  key )
        setkey( route.params.buget)
   }).catch(err => console.log(err))

   useEffect(() => {
    if(company && user && key > 0) {
        getBuge.get(`company=${company}&userID=${user}&doctID=${route.params.doctor}&bugetID=${key}&type=d`).then(rpt => {
            if (rpt.status) {
                setNCitas(rpt.data[0].ncitas.toString())
                setFCitas(rpt.data[0].fcitas.toString())
                setState(parseInt(rpt.data[0].state))
                if(parseInt(rpt.data[0].blanqui) > 0)
                  setPlanqui(parseInt(rpt.data[0].blanqui).toString())
                if(parseInt(rpt.data[0].rteSupe) > 0)
                  setPteSupe(parseInt(rpt.data[0].rteSupe).toString())
                if(parseInt(rpt.data[0].rteInfe) > 0)
                  setPteInfe(parseInt(rpt.data[0].rteInfe).toString())
                setToken(rpt.data[0].fcmToken.toString())
                setRteInfe(parseInt(rpt.data[0].rteInfe) > 0 ? true : false)
                setRteSupe(parseInt(rpt.data[0].rteSupe) > 0 ? true : false)
                setBlanqui(parseInt(rpt.data[0].blanqui) > 0 ? true : false)
                setTotal( constformatNumber(parseInt(rpt.data[0].blanqui) + parseInt(rpt.data[0].rteSupe) + parseInt(rpt.data[0].rteInfe) + ( parseInt(rpt.data[0].price) * parseInt(rpt.data[0].ncitas) ),0) )
            }
            else {
                navigation.goBack()
                msgNoti(rpt.data)
            }
        })           
    }
   }, [key])

   const handleToUpdate = (someArg) => {
        setFCitas(someArg);
   }

   const Notification = (token,body,callback) => {
    sendNotification.get(`company=${company}&token=${token}&body=${body}`).then(rpt => {
      callback( rpt );
    })
   }

   const constformatNumber = ( num, fixed ) => { 
        var decimalPart;

        var array = Math.floor(num).toString().split('');
        var index = -3; 
        while ( array.length + index > 0 ) { 
            array.splice( index, 0, '.' );              
            index -= 4;
        }

        if(fixed > 0){
            decimalPart = num.toFixed(fixed).split(".")[1];
            return array.join('') + "," + decimalPart; 
        }
        return array.join(''); 
   }

   return(
       <View style={styles.container}>
            <View style={{alignContent: 'flex-end', alignItems: 'flex-end'}} >
               <Text style={{height: 80, marginTop: -25}}>
                    <Image source={icono}/>
               </Text>
           </View>

           <View style={{alignContent: 'center', alignItems: 'center'}} >
                <Image style={styles.img} source={{ uri: lUser.uri }} />

                <Text style={{color:"#FFF", marginTop: 15}}>N° Ppto ({route.params.buget}) </Text>
           </View>           

           <View style={{
                    alignContent: 'center', 
                    alignItems: 'center', 
                    marginBottom: 15, 
                    marginTop: 10,
                    flexDirection: 'row',
                }}
           >
               <TouchableOpacity
                  onPress={() => {
                    navigation.goBack()
                  }}
               >
                <MaterialCommunityIcons
                    name="chevron-left"
                    size={40}
                    color="#fff"
                    style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>

               <Text style={{fontSize: 28, color: "#d3df4d", marginLeft: (Dimensions.get('window').width / 2) * 0.4}} >
                    $ Presupuestos
               </Text>

               <TouchableOpacity style={{position: 'absolute', right: 0}} >
                    <MaterialCommunityIcons
                        name="plus"
                        size={40}
                        color="#fff"
                        style={{marginLeft:5, color: "#d3df4d"}}
                    />
               </TouchableOpacity>
           </View>

           <View style={{ 
               justifyContent: 'center', 
               alignItems:'center', 
               backgroundColor: "#e5e5e6",
               height: 35,
            }}>
               <Text style={{ color: "#2f4f4f", fontSize: 20 }}>
                   Presupuesto Ortodoncia
               </Text>

           </View>
          
           {/* # Citas */}
           <View style={{ 
                backgroundColor: "#312b2d",
                height: 35,
                marginTop: 7,
                paddingLeft: 6,
                flexDirection: 'row',
            }}>
          
                <Text style={{ color: "#d3df4d", fontSize: 20, marginRight: 5, fontWeight: 'bold' }}>
                    # Citas:
                </Text>

                <TextInput
                    style={{ color: "#FFF", 
                            borderColor: "#000", 
                            borderWidth: 1,
                            fontSize: 20,
                            padding: 5,
                            paddingLeft: 14,
                            width: 50
                        }}
                    value={ncitas}
                    keyboardType='numeric'
                    editable={true}
                    onChangeText={setNCitas}
                    editable={false}
                />

                <TouchableOpacity
                    style={{ 
                             backgroundColor: "#000", 
                             padding: 5,
                             paddingLeft: 14,
                             width: 100,
                             position: 'absolute',
                             height: 40,
                             alignItems: 'center',
                             justifyContent: 'center',
                             right: 0,
                             bottom: 0,
                            }}                  
                >
                    <Text
                        style={{ color: "#d3df4d", 
                                 fontSize: 18,
                               }}
                    > 
                        N° 
                    </Text>
                </TouchableOpacity>
               
           </View>
           
           {/* Frecuencia de Citas */}
           <View style={{ backgroundColor: "#312b2d",
                          height: 70,
                          marginTop: 5,
                          paddingLeft: 6,
                          flexDirection: 'column',
            }}>
              <Text style={{ color: "#d3df4d", fontSize: 20, marginRight: 15, fontWeight: 'bold' }}>
                 Frecuencia de citas:    
                <Text style={{ color: "#d3df4d", fontSize: 13}}>
                &nbsp;&nbsp;(espacio días entre citas)
                </Text>
              </Text>

              <View style={{marginTop: 10}}>
                    <RadioButton PROP = {PROP} handleToUpdate = {handleToUpdate} val = {fcitas} active = {true} />
              </View>

           </View>
           
           {/* Retenedor Inferior */}
           <View style={{ 
                backgroundColor: "#312b2d",
                height: 45,
                marginTop: 7,
                paddingLeft: 22,
                flexDirection: 'row',
            }}>

            <CheckBox
                style={{flex: 1, padding: 10,  transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],}}
                onClick={()=> {
                    setRteInfe( !rteInfe )
                }}
                isChecked={rteInfe}
                rightText={"Retenedor Inferior: $" + constformatNumber(pteInfe)}
                rightTextStyle={{color: "#FFF", fontSize: 20}}
                checkBoxColor={"#FFF"}
                checkedCheckBoxColor={"#d3df4d"}
                CheckboxRadius={100}
                disabled={true}
            />

           </View>
           
           {/* Retenedor Superior */}
           <View style={{ backgroundColor: "#312b2d",
                          height: 45,
                          marginTop: 7,
                          paddingLeft: 22,
                          flexDirection: 'row',
            }}>

            <CheckBox
                style={{flex: 1, padding: 10,  transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],}}
                onClick={()=> {
                    setRteSupe( !rteSupe )
                }}
                isChecked={rteSupe}
                rightText={"Retenedor Superior: $" + constformatNumber(pteSupe)}
                rightTextStyle={{color: "#FFF", fontSize: 20}}
                checkBoxColor={"#FFF"}
                checkedCheckBoxColor={"#d3df4d"}
                CheckboxRadius={100}
                disabled={true}
            />

           </View>
           
           {/* Gengivoplastia */}
           <View style={{ backgroundColor: "#312b2d",
                          height: 70,
                          marginTop: 7,
                          paddingLeft: 22,
                          flexDirection: 'row',
            }}>

            <CheckBox
                style={{flex: 1, padding: 10,  transform: [{ scaleX: 1.1 }, { scaleY: 1.1 }],}}
                onClick={()=> {
                    setBlanqui( !blanqui )
                }}
                isChecked={blanqui}
                rightText={"Gengivoplastia y blanquiamiento: $" + constformatNumber(planqui)}
                rightTextStyle={{color: "#FFF", fontSize: 20}}
                checkBoxColor={"#FFF"}
                checkedCheckBoxColor={"#d3df4d"}
                CheckboxRadius={100}
                disabled={true}
            />

           </View>
           
           {/* Total */}
           <View style={{ backgroundColor: "#312b2d",
                          height: 40,
                          marginTop: 7,
                          paddingLeft: 6,
                          flexDirection: 'row',
            }}>

          
                <Text style={{ color: "#d3df4d", fontSize: 25, marginRight: 5, fontWeight: 'bold' }}>
                    Valor Total :
                </Text>

                <Text
                  style={{fontSize: 25, color: "#FFF", position: 'absolute', right:5}}
                >
                    ${total}
                </Text>
               
           </View>
           
           {/* Guardar */}
           <View style={{alignItems: 'flex-end', marginTop: 5}} >
                <TouchableOpacity
                    style={{backgroundColor: "#d3df4d", width: 100, height: 40 ,padding: 6, borderRadius: 15}}
                    onPress={() =>
                    {
                        const bugt = {
                            bugetID: key, userID: user,
                            doctID: route.params.doctor
                        }

                        approved.post( bugt ).then(rpt => {
                            msgNoti( rpt.data )
                            if(!state && token != "") {
                                let msg = `El paciente aprobo el presupuesto`
                                Notification(token,msg,function(){})
                            }
                            navigation.goBack()
                        }).catch(err => {
                            console.log(err);
                        })
                    }}
                >
                    <Text style={{fontSize: 20, color: "#000", fontWeight: 'bold', width: 100, paddingStart: 8}}>
                        Aprobar
                    </Text>
                </TouchableOpacity>
            </View>

       </View>
   )
}

export default checkPpto