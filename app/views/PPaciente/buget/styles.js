import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
    },
    subnontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        width: 120,
        height: 120,
        borderColor: "#D9DA08",
        borderWidth: 2,
        borderRadius: 60,
    }
})