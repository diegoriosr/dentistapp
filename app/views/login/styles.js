import { StyleSheet, Dimensions } from 'react-native'
const window = Dimensions.get('window')

export const IMAGE_HEIGHT = window.width / 5
export const IMAGE_HEIGHT_SMALL = window.width / 6
export const IMAGE_PADDING = window.width / 4
export const IMAGE_PADDING_SMALL = 120
export const IMAGE_MARGIN_SMALL = 120
export const IMAGE_MARGIN = 220

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
        paddingVertical: 50,
        paddingHorizontal:30
    },
    subnontainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: IMAGE_PADDING,
    },
    subnontainerbody: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: IMAGE_MARGIN,
    },
    subnontainerbody3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 50,
    },
    img: {
        width: 150,
        height: IMAGE_HEIGHT,
        resizeMode: 'contain',
        borderColor: "#d3df4d",
    }
})