import React , { useState, useEffect  } from  'react';
import { View, Text, TouchableOpacity, Animated , Keyboard } from 'react-native';
import { useDispatch   } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useFocusEffect } from '@react-navigation/native'
//
import userImg from '../../assets/icons/logo.png'
import { login, forgotPass, msgNoti } from '../../settings/global'
//components
import Button from '../../components/button'
import Input from '../../components/input'
import { AuthContext } from "../../settings/context"
//assets
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL, IMAGE_PADDING, IMAGE_PADDING_SMALL, IMAGE_MARGIN_SMALL, IMAGE_MARGIN } from './styles'

const Login = (props) => {

    const dispatch = useDispatch()
    const { signIn } = React.useContext(AuthContext)    
    const [email, setEmail] = useState("") //dentistdoc@gmail.com ingdiegorios@gmail.com
    const [password, setPassword] = useState("")
    const [activo, setActivo] = useState(false)
    const [token, setToken] = useState()
    const [keyboardHeight, setkeyboardHeight] = useState(new Animated.Value(0))
    const [imageHeight, setimageHeight] = useState(new Animated.Value(IMAGE_HEIGHT))
    const [imagePadding, setimagePadding] = useState(new Animated.Value(IMAGE_PADDING))
    const [imageMargin, setimageMargin] = useState(new Animated.Value(IMAGE_MARGIN))

    setStringValue = async (value) => {
        try {
          await AsyncStorage.setItem('user', JSON.stringify(value))
        } catch(e) {  }
    }

    useEffect(() => 
    {
        const keyboardWillShowSub = Keyboard.addListener( 'keyboardDidShow', keyboardWillShow)
        const keyboardWillHideSub = Keyboard.addListener( 'keyboardDidHide', keyboardWillHide)

        return () => {
            keyboardWillHideSub.remove()
            keyboardWillShowSub.remove()
        }
    }, []);

    const keyboardWillShow = (event) => {
       Animated.parallel([
        Animated.timing(keyboardHeight, {
          duration: event.duration,
          toValue: event.endCoordinates.height,
          useNativeDriver: false,
        }),
        Animated.timing(imageHeight, {
          duration: event.duration,
          toValue: IMAGE_HEIGHT_SMALL,
          useNativeDriver: false,
        }),
        Animated.timing(imagePadding, {
            duration: event.duration,
            toValue: IMAGE_PADDING_SMALL,
            useNativeDriver: false,
        }),
        Animated.timing(imageMargin, {
            duration: event.duration,
            toValue: IMAGE_MARGIN_SMALL,
            useNativeDriver: false,
        }),
       ]).start()
    };
    
    const keyboardWillHide = (event) => {
        Animated.parallel([
            Animated.timing(keyboardHeight, {
                duration: event.duration,
                toValue: 0,
                useNativeDriver: false,
            }),
            Animated.timing(imageHeight, {
                duration: event.duration,
                toValue: IMAGE_HEIGHT,
                useNativeDriver: false,
            }),
            Animated.timing(imagePadding, {
                duration: event.duration,
                toValue: IMAGE_PADDING,
                useNativeDriver: false,
            }),
            Animated.timing(imageMargin, {
                duration: event.duration,
                toValue: IMAGE_MARGIN,
                useNativeDriver: false,
            }),
        ]).start();
    };

    useFocusEffect (
        React.useCallback(() => 
        {
            AsyncStorage.getItem("user").then(val => {
                if( val ) {
                    if( JSON.parse(val).name ) {                                            
                        setActivo(true)
                        if( JSON.parse(val).email && JSON.parse(val).password ) {
                            AsyncStorage.getItem("fcmToken").then(valToken => {
                                if( valToken ) {
                                    const usr = {email: JSON.parse(val).email, password: JSON.parse(val).password, token: valToken}
                                    login.post( usr ).then(rpt => {
                                        if (rpt.status)
                                        {
                                            let obj = {
                                                id: rpt.data.userID,
                                                name: rpt.data.userName,
                                                uri: rpt.data.pathURL,
                                                email: rpt.data.email,
                                                company: rpt.data.company,
                                                role: rpt.data.role,
                                                resaleID: rpt.data.resaleID,
                                                password
                                            }
                                            setStringValue(obj)
                                            dispatch({ type: 'setUser', payload: obj })
                                            signIn()
                                        }
                                        else
                                            console.log(rpt.data)
                                    }).catch(error => console.log( error ))
                                }
                            }).catch(err => console.log(err))
                        }
                    }
                }
            }).catch(err => console.log(err))
        }, [keyboardHeight])
    );

    AsyncStorage.getItem("fcmToken").then(val => {
        if( val ) {
            setToken(val)
        }
    }).catch(err => console.log(err))
    
    return(
      <>        
        <View style={styles.container}>

            <Animated.View style={[styles.subnontainer, {marginBottom: imagePadding }]}>
               <Animated.Image source={userImg} style={[styles.img, { height: imageHeight }]} />
            </Animated.View>

            <Animated.View style={[styles.subnontainerbody, {marginBottom: imageMargin }]}>

                <Input
                    title="Email"
                    custom={{
                        value:email,
                        onChangeText: val => setEmail(val)
                    }}
                />

                <Input
                    title="Password"
                    custom={{
                        value:password,
                        onChangeText: val => setPassword(val),
                        secureTextEntry: true
                    }}
                />

                <Button
                    style={{backgroundColor: '#1f1f1f', width: "10%"}}
                    title="Iniciar Sesión"
                    action={() => {
                        const usr = {email, password, token}

                        login.post( usr ).then(rpt => {
                            if (rpt.status) 
                            {
                                let obj = {
                                    id: rpt.data.userID,
                                    name: rpt.data.userName,
                                    uri: rpt.data.pathURL,
                                    email: rpt.data.email,
                                    company: rpt.data.company,
                                    role: rpt.data.role,
                                    resaleID: rpt.data.resaleID,
                                    password
                                }
                                setStringValue(obj)
                                dispatch({ type: 'setUser', payload: obj })
                                signIn()
                            }
                            else {
                              msgNoti(rpt.data)
                            }
                        }).catch(error => console.log(error))
                    }} 
                />

            </Animated.View>
        
            {( !activo ?
                <View style={styles.subnontainerbody3}>
                    <TouchableOpacity
                            style={{backgroundColor:'#d3df4d', width:200, height:50, borderRadius:100, alignItems:'center'}}
                            onPress={() => {
                                props.navigation.navigate('CreateUser', { ID: 1 })
                            }}
                        >
                            <Text style={{ color:"#000", fontWeight: 'bold', marginTop: 15, fontSize: 16 }}>
                                Activar Cuenta
                            </Text>
                    </TouchableOpacity>
                </View>    
            :
                <View>
                    <TouchableOpacity
                    style={{alignSelf: 'flex-end'}}
                    onPress={() => { 
                        AsyncStorage.getItem("user").then(val => {
                            if( val ) {
                                const usr = {
                                    userID: JSON.parse(val).id,
                                    company: JSON.parse(val).company,
                                    name: JSON.parse(val).name,
                                    email: JSON.parse(val).email,
                                    resaleID: JSON.parse(val).resaleID,
                                }

                                forgotPass.post( usr ).then(rpt => {
                                    if (rpt.status) {
                                        props.navigation.navigate("CreateUser", { ID: 1, auth: rpt.data.auth  })
                                    }
                                    else
                                        console.log(rpt.data)
                                }).catch(error => console.log(error))
                            }
                        }).catch(err => console.log(err))
                    }}
                    >
                        <Text style={{ color:"#FFF", marginTop: 15, fontSize: 18 }}>
                            ¿Cambiar Password?
                        </Text>
                    </TouchableOpacity>  
                </View>
            )}
        </View>
      </>
    );
}

export default Login;