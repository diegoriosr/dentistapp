import React, { Component } from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker'
import AsyncStorage from '@react-native-async-storage/async-storage'
//Assets
import image from '../../assets/dentist.png'
import { GetByToken, signin, requestCameraPermission, server, sendNotification, msgNoti } from '../../settings/global'
//Component
import { uploadFiles } from '../../utils/uploadFiles'
import { FloatingAction } from "react-native-floating-action"

class CreateUser extends Component {

    constructor(props) {

        super(props)

        this.state = {
            userID: 0,
            authCode: "",
            email: '',
            password: '',
            resaleID: '',
            confirm: '',
            avatar: null,
            photo: null,
            company: '',
            userName: '',
            fcmToken: '',
            role: ''
        }
    }

    setStringValue = async (value) => {
        try {
          await AsyncStorage.setItem('user', JSON.stringify(value))
        } catch(e) {  }
    }
    
    componentDidMount() {
        if( this.props.route.params.auth ) {
            this.setState({ authCode: this.props.route.params.auth })
        }
    }

    render() 
    {
        const { authCode, email, password, confirm, avatar, photo, resaleID, company, userID, userName, role, fcmToken } = this.state

        const setStringValue = async (value) => {
            try {
              await AsyncStorage.setItem('user', JSON.stringify(value))
            } catch(e) {  }
        }

        const Notification = (token,body,callback) => {
            if( token != "" ) {
                sendNotification.get(`company=${company}&token=${token}&body=${body}`).then(rpt => {
                    callback( rpt );
                })
            }
        }

        return <View style={styles.container}>

                <ScrollView>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                        <TouchableOpacity
                          onPress={async () => {

                            let rp = await requestCameraPermission();

                            if( rp ) {

                                let options = {
                                    mediaType: 'photo', cameraType: 'front',
                                    maxWidth: 300, maxHeight: 300, quality: 1,
                                 }
    
                                launchCamera(options, (res) => {
                                    this.setState({ avatar: res.uri, photo: res })
                                });
                            }

                          }}
                        >
                            <Image source={avatar ? { uri: avatar } : image} style={styles.image} />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.title}>Código activación:</Text>

                    <View style={{flexDirection:"row"}}>

                        <TextInput
                            style={styles.inputRow}
                            value={authCode}
                            onChangeText={val => this.setState({ authCode: val })}
                            onBlur={() => {
                                const usr = {authCode }
                                GetByToken.post( usr ).then(rpt => {
                                    if( rpt.status ) {
                                        this.setState({
                                            resaleID: rpt.data[0].resaleID.toString(),
                                            userID: rpt.data[0].id.toString(), 
                                            email: rpt.data[0].email,
                                            company: rpt.data[0].company,
                                            userName: rpt.data[0].userName,
                                        })

                                        if( rpt.data[0].role.toString() == "patient" )
                                           this.setState({ fcmToken: rpt.data[0].fcmToken.toString(), role: 'patient' })
                                    } else {
                                      msgNoti(rpt.data)
                                    }
                                })
                            }}
                        />

                        <TouchableOpacity 
                           style={styles.buttonRow}
                           onPress={() => {
                              if( authCode != "" && authCode != 'undefined' && authCode != undefined )
                              {
                                const usr = {authCode }
                                GetByToken.post( usr ).then(rpt => {
                                    if( rpt.status ) 
                                    {
                                        this.setState({
                                            resaleID: rpt.data[0].resaleID.toString(),
                                            userID: rpt.data[0].id.toString(), 
                                            email: rpt.data[0].email,
                                            company: rpt.data[0].company,
                                            userName: rpt.data[0].userName,
                                        })

                                        if( rpt.data[0].role.toString() == "patient" )
                                           this.setState({ fcmToken: rpt.data[0].fcmToken.toString(), role: 'patient' })
                                    } else {
                                      msgNoti(rpt.data)
                                    }
                                })
                              } 
                              else {
                                msgNoti("Ingrese Código de activación")
                              }
                           }}                       
                        >
                            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Validar</Text>
                        </TouchableOpacity>
                    
                    </View>

                    <Text style={styles.title}>Email:</Text>

                    <TextInput
                        style={styles.input}
                        value={email}
                        editable={false}
                        onChangeText={val => this.setState({ email: val })}
                    />

                    <Text style={styles.title}>Identificación:</Text>
                    <TextInput
                        style={styles.input}
                        value={resaleID}
                        editable={false}
                        onChangeText={val => this.setState({ resaleID: val })}
                    />

                    <Text style={styles.title}>Password:</Text>
                    <TextInput
                        secureTextEntry
                        style={styles.input}
                        value={password}
                        onChangeText={val => this.setState({ password: val })}
                    />

                    <Text style={styles.title}>Confirmar password:</Text>
                    <TextInput
                        secureTextEntry
                        style={styles.input}
                        value={confirm}
                        onChangeText={val => this.setState({ confirm: val })}
                    />

                </ScrollView>
                
                <FloatingAction
                    iconWidth={40}
                    iconHeight={40}
                    buttonSize={60}
                    floatingIcon={
                        require("../../assets/icons/save.png")
                    }
                    color={"#d3df4d"}
                    iconColor={"#FFF"}
                    onOpen={() => {
                        const uri = `${server}/img/${email}/${email}.png`
                            
                        const usr = {authCode, email, password, confirm, pathURL: uri, 
                                     resaleID, company, userID, active: true, tipo: 'a',
                                     status: 1
                                    }

                        let obj = { id: userID, name: userName, password,
                            pathURL: uri, email, company, role: "patient"
                        }

                        setStringValue(obj)

                        signin.post( usr ).then( (rpt) => {
                            if( rpt.status ) {
                                msgNoti("Cuenta activa con exito")

                                if( fcmToken != "" && role == "patient" ) {
                                    let msg = `El Paciente ${this.state.userName} activo su cuenta`
                                    Notification(this.state.fcmToken,msg,function(){})
                                }

                                if( photo != null ) {
                                    uploadFiles(photo, email)
                                    this.props.navigation.goBack()
                                } else {
                                    this.props.navigation.goBack()
                                }
                            } 
                            else {
                              msgNoti(rpt.data[0].data)
                            }
                        }).catch( err => { alert(err) })
                    }}
                />
            </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
        paddingVertical: 10,
        paddingHorizontal: 30
    },
    title: {
        color: "#d3df4d",
        fontSize: 16,
        fontWeight: "bold",
        marginVertical: 8,
    },
    input: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "100%",
        paddingHorizontal: 10,
        color: "#FFF",
        borderRadius: 5
    },
    inputRow: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "60%",
        paddingHorizontal: 10,
        color: "#FFF",
        borderRadius: 5
    },
    button: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "70%",
        marginTop: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginLeft: 50
    },
    buttonRow: {
        borderWidth: 1.5,
        borderColor: "#fff",
        backgroundColor: "#d3df4d",
        height: 40,
        width: "36%",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginLeft: 10
    },
    image: {
        marginVertical: 8,
        height: 150,
        width: 150,
        borderRadius: 75,
        //resizeMode: 'contain',
        borderColor: "#d3df4d",
        borderWidth: 2,
    }
})

export default CreateUser