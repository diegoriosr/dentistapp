import React, { Component, useRef } from 'react'
import {  ActivityIndicator, StyleSheet, View, Animated, Dimensions } from 'react-native'
//
import userImg from '../../assets/icons/logo.png'

class ImageLoader extends Component {
   state = {
       opacity: new Animated.Value(0),
       animation: new Animated.Value(1)
   }   

   onLoad = () => 
   {
       Animated.sequence([
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }),
        Animated.timing(this.state.opacity, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true,
        }),
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }),
        Animated.timing(this.state.animation, {
            toValue: -210,
            duration: 1500,
            useNativeDriver: true,
        }),
       ]).start();
   }

   render() {
       return (
           <Animated.Image
             onLoad={this.onLoad}
             {...this.props}
             style={[
                this.props.style,
                {
                    transform: [
                       { translateY: this.state.animation },
                       {
                            scale: this.state.opacity.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0.85, 1]
                            })
                       }
                    ],
                    opacity: this.state.opacity,
                }
             ]}
           />
       )
   }
}

function Loading({ loading, children }) 
{
   if(loading) {
        return (
            <View style={styles.container}>
                <ImageLoader
                    style={styles.img}
                    source={userImg}
                />
                <ActivityIndicator size="small" color="#d3df4d" textContent={'Loading...'} />
            </View>
        )
   }
   return children
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000",
        justifyContent: 'center',
        alignItems: 'center'
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    img: {
        width: 150,
        height: 80,
        borderColor: "#d3df4d",
        tintColor: "#d3df4d"
    }
})

export default Loading

/**
 * Component
 * prop loading
 */