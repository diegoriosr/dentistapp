import React from 'react'
import { Modal, TouchableOpacity, View } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import { styles } from './styles'

function ModalCustom({onDismiss = () => null, onShow = () => null, visible, children, onClose }) {

    return(
        <Modal
            animationType="slide"
            onDismiss={() => onDismiss}
            onShow={() => onShow}
            transparent
            visible={visible}
        >
            <View style={styles.container} >

                <View style={styles.subContainer} >

                    <View style={styles.headerContainer} >

                        <TouchableOpacity
                            onPress={ onClose }
                        >
                            <MaterialCommunityIcons
                                name="close"
                                size={40}
                                color="#fff"
                                style={{marginLeft:5, color: "#d3df4d"}}
                            />

                        </TouchableOpacity>

                    </View>

                    { children }

                </View>

            </View>

        </Modal>
    )
}
export default ModalCustom