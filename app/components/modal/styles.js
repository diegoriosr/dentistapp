import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'rgba(1,1,1, 0.7)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    subContainer: {
        height: '90%',
        width: '100%',
        backgroundColor: '#000',
    },
    headerContainer: {
        height: 45,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 10
    },
})