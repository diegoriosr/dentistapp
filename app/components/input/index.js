import React, { Fragment } from 'react'
import { TextInput, Text } from 'react-native'

import { styles } from './styles'

function Input({ title, value, custom }) {
   return (
        <Fragment>
            <Text style={styles.title}>{title}</Text>
            <TextInput
                style={styles.text}
                value={value}
                {...custom}
            />
        </Fragment>
   )
}

export default Input