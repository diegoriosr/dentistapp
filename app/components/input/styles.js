import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    title: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 10,
    },
    text: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 39,
        width: "100%",
        paddingHorizontal: 10,
        paddingVertical: 0,
        color: "#FFF",
        backgroundColor: '#0F0E0E',
        backgroundColor: "#090909",
        borderRadius: 10
    }
})