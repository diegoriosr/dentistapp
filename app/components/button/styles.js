import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    btn: {
        borderWidth: 1,
        borderColor: "#d3df4d",
        height: 40,
        width: "70%",
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10
    },
    title: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 10
    },
})