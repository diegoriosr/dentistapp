import React, { useEffect, useState } from 'react'
import { View, Image, TouchableOpacity, Text } from 'react-native'
import { useSelector } from 'react-redux'
import { launchCamera } from 'react-native-image-picker'
import { requestCameraPermission } from '../../settings/global'

import {  DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer'

// Assets
import logout from '../../assets/icons/logout.png'
import user from '../../assets/icons/user.png'

//Component
import { uploadFiles } from '../../utils/uploadFiles'

import { AuthContext } from "../../settings/context"

export default function DrawerContent(props) {

    const { signOut } = React.useContext(AuthContext)
    const user = useSelector((state) => state.user)

    const [avatar, setAvatar] = useState('')
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')

    useEffect(() => {      
      setAvatar( user.uri + '?' + Math.round((new Date).getTime() / 1000) )
      setEmail( user.email )
      setName( user.name )
    }, [])

    return (
       <View style={{ flex: 1, backgroundColor:'#010101' }} >

           <TouchableOpacity
               onPress={async () => {
 
                let rp = await requestCameraPermission();

                if(rp) {
                  
                    let options = {
                        mediaType: 'photo', cameraType: 'front',
                        maxWidth: 300, maxHeight: 300, quality: 0.9,
                    }

                    launchCamera(options, (res) => {
                        if(!res.didCancel){
                          setAvatar(res.uri)
                          uploadFiles(res, email)
                        }
                    });
                }
              }}
           >
              <View style={{height:200, width: '100%', justifyContent: 'center', alignItems: 'center'}} >
                  <Image source={avatar ? {uri: avatar} : user } 
                         style={{width:150, height:150, borderRadius: 75}}
                  />

                  <Text style={{ color: '#FFF', marginTop: 10 }}> {name} </Text>
              </View>
 
            
           </TouchableOpacity>

           <DrawerContentScrollView {...props}>
             <DrawerItemList 
                 {...props}
                 activeBackgroundColor="#D9DA08"
                 labelStyle={{color: "#FFF"}}
             />
           </DrawerContentScrollView>

           <DrawerItem
              label="Cerrar Sesión"
              labelStyle={{color: "#FFF"}}
              style={{marginBottom: 25}}
              icon={() => (
                  <Image
                    style={{ width:25, height:25, tintColor: '#FFF' }} 
                    source={logout}
                  />
              )}
              onPress={() => {
                signOut()
              }}
           />
           
       </View>
    );
}