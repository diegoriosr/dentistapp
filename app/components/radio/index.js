import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

export default class RadioButton extends Component {
	state = {
		value: '8',
		active: true
	};

	componentDidMount(){
		const { val, active } = this.props
		this.setState({ value: val, active });
	}
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		if(  prevState.value != this.props.val ) {
			const { val } = this.props
		    this.setState({ value: val });
		}
		
	}
	
	render() {
		const { value, active } = this.state
		const { PROP, handleToUpdate } = this.props

		return (
			<View style={{flexDirection: 'row'}}>
				{PROP.map(res => {
					return (
						<View key={res.key} style={styles.container}>
							<TouchableOpacity
								style={styles.radioCircle}
								disabled={active}
								onPress={() => {
                                    this.setState({
                                        value: res.key
									});
									handleToUpdate(res.key)
								}}>
                                {value === res.key && <View style={styles.selectedRb} />}
							</TouchableOpacity>
                            <Text style={styles.radioText}>{res.text}</Text>
						</View>
					);
				})}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
        alignItems: 'center',
        flexDirection: 'row',
		justifyContent: 'space-between',
	},
    radioText: {
        marginLeft: 10,
        fontSize: 20,
        color: '#FFF',
        fontWeight: '700',
        marginRight: 10
    },
	radioCircle: {
		height: 20,
		width: 20,
		borderRadius: 5,
		borderWidth: 2,
		borderColor: '#CCC',
		alignItems: 'center',
		justifyContent: 'center',
        marginLeft: 10
	},
	selectedRb: {
		width: 15,
		height: 15,
		borderRadius: 15,
		backgroundColor: '#d3df4d',
    },
    result: {
        marginTop: 20,
        color: 'white',
        fontWeight: '600',
        backgroundColor: '#F3FBFE',
    },
});