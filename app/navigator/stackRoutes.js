import Login from '../views/login'
import CreateUser from '../views/createUser'
import Patient from '../views/POrtodoncista/pacient'
import Citas from '../views/POrtodoncista/citas'
import CitasxPpte from '../views/POrtodoncista/citasxPtte'

export default {
    Login,
    CreateUser,
    Patient,
    CitasxPpte,
    Citas
}