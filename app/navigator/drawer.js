import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

//components
import DrawerComponent from '../components/drawer'

//screen
import Routes from './drawerRoutes'

const Drawer = createDrawerNavigator();

export default function Drawernavigator() {
    
  return (
      <Drawer.Navigator
        headerMode="screen"
        drawerContent={(props) => <DrawerComponent {...props}/> }
        initialRouteName={"agenda"}
      >
          <Drawer.Screen 
            name="AddPatient" 
            component={Routes.AddPatient} 
            initialParams={{ user: {id: 0} }}
            onPress={() => { }}
            options={{
                 title: "Nuevo Pacientes",
                 drawerIcon: ({ color }) => (
                  <MaterialCommunityIcons
                    name="account-plus-outline"
                    size={24}
                    color="#fff"
                    style={{marginLeft:-4, color:"#d3df4d", marginEnd: -20}}
                  />  
                )
            }} 
          />
          <Drawer.Screen name="misCitas" component={Routes.misCitas} options={{title: "Agendamiento Citas"}} />
          <Drawer.Screen name="agenda" component={Routes.agenda} options={{drawerLabel: "Mis Citas"}} />
          <Drawer.Screen name="MisPacientes" component={Routes.MisPacientes} options={{ drawerLabel: 'Mis Pacientes' }} />
          <Drawer.Screen 
            name="Incumplidos" 
            component={Routes.incumplidos}
            options={{ 
              drawerLabel: 'Pacientes Incumplidos',
              drawerIcon: ({ color }) => (
                  <MaterialCommunityIcons
                    name="alert-outline"
                    size={24}
                    color="#fff"
                    style={{marginLeft:-4, color: "red", marginEnd: -20}}
                  />  
              )
            }}
          />
      </Drawer.Navigator>
  );
}