import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
//components
import DrawerComponent from '../components/drawer'
//screen
import BugetList from '../views/PPaciente/bugets'
import pctHome from '../views/PPaciente/home'
import QRCodeScannerScreen from '../views/POrtodoncista/qrCode'

const Drawer = createDrawerNavigator();

export default function Drawernavigator() {
  return (
      <Drawer.Navigator
        headerMode="screen"
        drawerContent={(props) => <DrawerComponent {...props}/> }
      >          
          <Drawer.Screen 
              name="pctHome" 
              component={pctHome} 
              options={{
                drawerLabel: 'Mis Citas',
                drawerIcon: ({ color }) => (
                    <MaterialCommunityIcons
                      name="calendar"
                      size={24}
                      color="#fff"
                      style={{marginLeft:-4, color: "#FE0000", marginEnd: -20}}
                    />  
                  )
                }} 
          />
          <Drawer.Screen name="BugetList" component={BugetList} options={{ title: "Presupuestos / Citas" }} />
          <Drawer.Screen 
              name="Anunciar" 
              component={QRCodeScannerScreen}
              options={{ 
                drawerLabel: 'Anunciar Llegada',
                drawerIcon: ({ color }) => (
                  <MaterialCommunityIcons
                    name="qrcode"
                    size={24}
                    color="#fff"
                    style={{marginLeft:-4, color: "#D9DA08", marginEnd: -20}}
                  />  
                )
            }}
          />
      </Drawer.Navigator>
  );
}