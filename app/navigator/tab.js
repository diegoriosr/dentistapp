import * as React from 'react'
import { Text, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import Citas from '../views/POrtodoncista/citas'

function SettingsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator()

export default function TQoute({route, navigation}) {
  return (
      <Tab.Navigator
        //initialRouteName="Cotizacion"
        //activeColor="#D9DA08"
        //barStyle={{ backgroundColor: '#336699' }}
        tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
            style: {
                backgroundColor: 'black',
             },
             labelStyle: {
                fontSize: 13,
             },
        }}
      >
        <Tab.Screen 
           name="Cotizacion" 
           component={Citas} 
           initialParams={{user: route.params.user, action: route.params.action, bugID: route.params.bugID }}
           options={{
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="home" color={color} size={40} />
            ),
          }}
         />
        <Tab.Screen 
           name="Historial" 
           component={SettingsScreen} 
           options={{
            tabBarLabel: '',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="account" color={color} size={40} />
            ),
          }}
        />
      </Tab.Navigator>
  );
}