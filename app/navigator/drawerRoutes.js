import AddPatient from '../views/POrtodoncista/addPatient'
import Citas from '../views/POrtodoncista/citas'
import MisPacientes from '../views/POrtodoncista/misPacientes'
import misCitas from '../views/POrtodoncista/misCitas'
import agenda from '../views/POrtodoncista/agenda'
import incumplidos from '../views/POrtodoncista/incumplidos'

export default {
  AddPatient,
  Citas,
  MisPacientes,
  misCitas,
  agenda,
  incumplidos
}