import React, { useState, useEffect } from 'react'
import { useStore  } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
//screen
import Routes from './stackRoutes'
import Home from './drawer'
import PHome from './drawerPP'
//
import BugetPatient from '../views/PPaciente/buget'
import MisCitas from '../views/PPaciente/citasxPtte'
//Component
import Loading from '../components/loading'
//
import { AuthContext } from "../settings/context"

const AuthStack = createStackNavigator();
const RootStack = createStackNavigator();

function AppStack({route, navigation}) {

  const store = useStore()

  const [role, setRole] = useState("")
  const [loading, setLoading] = useState(true)

  const AuthStackScreen = () => (
    <AuthStack.Navigator screenOptions={{
        headerTitleAlign: 'center',
        headerStyle: {
          backgroundColor: '#1f1f1f',
        },
        headerTintColor: '#d3df4d',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
        <AuthStack.Screen name="Login" component={Routes.Login} options={{headerShown: false }} />
        <AuthStack.Screen name="CreateUser" component={Routes.CreateUser} options={{title: "Activar Cuenta"} } />
    </AuthStack.Navigator>
  )

  const RootStackScreen = ({ role }) => (
      <RootStack.Navigator headerMode="none">
        { role == "doctor" ? (
          <>
            <RootStack.Screen           
              name="Home"
              component={Home}
              options={{
                animationEnabled: true
              }}
            />

            <RootStack.Screen           
              name="Patient"
              component={Routes.Patient}
              options={{
                animationEnabled: true
              }}
            />

            <RootStack.Screen           
              name="Citas"
              component={Routes.Citas}
              options={{
                animationEnabled: true
              }}
            />

           <RootStack.Screen           
              name="CitasxPpte"
              component={Routes.CitasxPpte}
              options={{
                animationEnabled: true
              }}
            />
          </>
        ) : role == "patient" ? (
          <>
            <RootStack.Screen              
              name="PHome"
              component={PHome}
              options={{
                animationEnabled: true
              }}
            />

            <RootStack.Screen 
              name="BugetPatient"
              component={BugetPatient}
              options={{
                animationEnabled: true
              }}
            />

            <RootStack.Screen 
              name="MisCitas"
              component={MisCitas}
              options={{
                animationEnabled: true
              }}
            />

          </>
        ) : (
          <RootStack.Screen
            name="Auth"
            component={AuthStackScreen}
            options={{
              animationEnabled: true
            }}
          />
        )}
      </RootStack.Navigator>
  )

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setRole( store.getState().user.role )
        setLoading(false)
      },
      signOut: () => {
        setLoading(true)
        setRole(null);
        setTimeout(() => { setLoading(false); }, 4000);
      }
    };
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 4000);
  }, []);

  return (
    <Loading loading={loading}>
      <AuthContext.Provider value={authContext}>
          <NavigationContainer>
              <RootStackScreen role={role} />
          </NavigationContainer>
        </AuthContext.Provider>
    </Loading>
  );
}

export default AppStack