import React from 'react'

function printHOC( WrppedComponent ) {
   return class NewComponent extends React.Component {

       print = param => console.log( { param } )

       render(){
           return <WrppedComponent print={this.print}/>
       }
   }
}

export default printHOC