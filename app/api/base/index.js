import axios from 'axios'
import { Service } from 'axios-middleware'

const _pathInst = 'https://dentistpocket.showupweb.com/'
//const _pathInst = 'http://10.1.0.206:3500'
//const _pathInst = 'http://192.168.43.209:3500'
const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json; charset=utf-8',
    'Accept': 'application/json',
    'x-dentist-key': "cfa115aad1msh305ef3ffc77f4c4p15fe57jsn6029c6a90445",
}
// https://pokemon-go1.p.rapidapi.com/

class Request {
    
    constructor(url, baseURL) {
        this.url = url
        this.baseURL = baseURL ||  _pathInst
        this.request = axios.create({
            baseURL: this.baseURL,
            headers: headers
        })
        
        const service = new Service(this.request);

        service.register({
            onResponseError(err = {} ) {
                const { response } = err;
                const resp = JSON.parse(response.data)
                      resp.status = false
                return resp;
            },
            onResponse(response) {
                const resp = JSON.parse(response.data)
                      resp.status = true
                      if( response.id )
                        resp.key = JSON.parse(response.id)
                return resp;
            }
        });
   }

   get(p) {
    return this.request({
        url: this.url + (p.length > 0 ? '?' + p : '')
    })
   }

   getByID(id) {
    return this.request({
        url: this.url + '/'+ id
    })
   }

   post(value) {
    return this.request({
        url: this.url,
        data: value,
        method: 'post',
    })
   }

   put(value, id) {
    return this.request({
        url: this.url + '/'+ id,
        data: value,
        method: 'put'
    })
   }

   delete(id) {
    return this.request({
        url: this.url + '/'+ id,
        method: 'delete'
    })
   }
}

export default Request