import { server } from '../settings/global'

export function uploadFiles(file, data) {

    const uri  = file.uri,
          type = file.type,
          name = file.fileName.trim() || 'image.png'

    const photo = {
        uri,
        type,
        name
    }
    const ts = Math.round((new Date).getTime() / 1000)

    const formData = new FormData()
    formData.append('file', photo)
    formData.append('timestamp', ts)
    formData.append('user',data)

    return fetch(`${server}/user/upload`, {
        method: 'POST',
        body: formData
    }).then(rpt => {
        console.log(rpt);
    }).catch(err => { console.log( err ); })
}