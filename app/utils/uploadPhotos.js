import { server } from '../settings/global'

export var uploadFiles = (file, data) => {

    const uri  = file.uri,
          type = file.type,
          name = file.fileName || 'image.png'

    const photo = {
        uri,
        type,
        name
    }
    const ts = Math.round((new Date).getTime() / 1000)

    const formData = new FormData()
    formData.append('file', photo)
    formData.append('timestamp', ts)
    formData.append('user',JSON.stringify(data))

    return fetch(`${server}/apponint/renderUpload`, {
        method: 'POST',
        body: formData
    }).then(rpt => {
        return {data: true, photo: ts+".png"}
    }).catch(err => { return {data: false, photo: err } })
}
//
export var uploadPpto = (file, data) => {
    const uri  = file.uri,
          type = file.type,
          name = file.fileName || 'image.png'

    const photo = {
        uri,
        type,
        name
    }
    const ts = Math.round((new Date).getTime() / 1000)

    const formData = new FormData()
    formData.append('file', photo)
    formData.append('timestamp', ts)
    formData.append('user',JSON.stringify(data))

    return fetch(`${server}/apponint/renderUploadPpto`, {
        method: 'POST',
        body: formData
    }).then(rpt => {
        return {data: true, photo: ts+".png"}
    }).catch(err => { return {data: false, photo: err } })
}